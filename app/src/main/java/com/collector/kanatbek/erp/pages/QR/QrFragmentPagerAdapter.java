package com.collector.kanatbek.erp.pages.QR;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class QrFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] { "Оплата по ИИН", "Оплата по серийному номеру" };
    private Context context;

    public QrFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override public int getCount() {
        return PAGE_COUNT;
    }

    @Override public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new QrByIin();
                break;
            case 1:
                fragment = new QrBySn();
                break;
        }
        return fragment;
    }

    @Override public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
