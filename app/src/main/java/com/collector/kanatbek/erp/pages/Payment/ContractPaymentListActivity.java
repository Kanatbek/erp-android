package com.collector.kanatbek.erp.pages.Payment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.collector.kanatbek.erp.models.Contract;
import com.collector.kanatbek.erp.models.ContractPaymentSchedule;
import com.collector.kanatbek.erp.R;
import com.collector.kanatbek.erp.Repository;
import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import com.collector.kanatbek.erp.utils.SmServiceDetailedAdapter;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ContractPaymentListActivity extends Activity {

    private ListView paymentGraph;
    private Integer contractId = 0;
    private String token = "";
    private ArrayList<ContractPaymentSchedule> contractPaymentList;
    private List<String> currencies;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_graph_list_activity);
        paymentGraph = findViewById(R.id.listView_payment_graph_list_id);

        Intent intent = getIntent();
        contractId = intent.getStringExtra("contractId") != null ?
                                Integer.parseInt(intent.getStringExtra("contractId")) : 0;

        getContractById();
    }

    public void getContractById() {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        token = explore.Explore(this, "TokenData", "token");

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + token).build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        Call<Contract> call = repository.getContractById(contractId,"contractForMobile");
        call.enqueue(new Callback<Contract>() {
            @Override
            public void onResponse(Call<Contract> call, Response<Contract> response) {
                if (response.isSuccessful() && response.body() != null) {
                    contractPaymentList = new ArrayList<>();
                    currencies = new ArrayList<>();
                    contractPaymentList = (ArrayList<ContractPaymentSchedule>) response.body().getContractPaymentSchedule();
                    for (int i=0;i<contractPaymentList.size();i++) {
                        currencies.add(response.body().getCurrency() != null ?
                                                response.body().getCurrency().getCurrency() : "");
                    }
                    fillContractPaymentList();
                } else {
                    Toasty.warning(getApplicationContext(),
                            "Не найдено контрактов",
                            Toast.LENGTH_SHORT,
                            true).show();
                }
            }
            @Override
            public void onFailure(Call<Contract> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void fillContractPaymentList() {
        final SmServiceDetailedAdapter adapter
                = new SmServiceDetailedAdapter(
                this
                , currencies
                , contractPaymentList
                , "");
        paymentGraph.setAdapter(adapter);
    }

}
