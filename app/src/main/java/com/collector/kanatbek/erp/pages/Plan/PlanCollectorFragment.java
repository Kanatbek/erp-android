package com.collector.kanatbek.erp.pages.Plan;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.collector.kanatbek.erp.utils.CalendarCalculator;
import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import com.collector.kanatbek.erp.models.CollectorVisit;
import com.collector.kanatbek.erp.Repository;
import com.collector.kanatbek.erp.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import java.util.*;

import es.dmoral.toasty.Toasty;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PlanCollectorFragment extends Fragment {

    BarChart barChart;
    BarData BARDATA;
    BarDataSet Bardataset ;
    private List<CollectorVisit> allVisits;
    ArrayList<BarEntry> BARENTRY ;
    ArrayList<String> BarEntryLabels;

    private String mToken = "";
    private Integer emplId = 0;

    private Integer totalSumm = 0;
    private Integer totalCashboxSumm= 0;
    private String currency = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_plan_fact, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        ExploreSharedMemory explore = new ExploreSharedMemory();
        getAllVisits();
        int mPlan = 0;
        int mFact = 0;
        mPlan = Integer.parseInt(explore.Explore(getActivity(), "TokenData", "plan_collector"));
        mFact = Integer.parseInt(explore.Explore(getActivity(), "TokenData", "fact_collector"));
        fillPlan(mPlan, mFact);

        Button img = Objects.requireNonNull(getActivity()).findViewById(R.id.visit_list_button);
        img.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), AllVisitHistory.class);
            startActivity(intent);
        });
    }

    public void fillPlan(Integer plan, Integer fact) {
        if (getView() != null) {
            barChart = Objects.requireNonNull(getActivity()).findViewById(R.id.chart_collector);

            barChart.setTouchEnabled(false);
            barChart.setDoubleTapToZoomEnabled(false);
            barChart.setDragEnabled(false);

            // style chart
            barChart.setBackgroundColor(getResources().getColor(R.color.white)); // use your bg color
            barChart.setDescription(null);
            barChart.setDrawGridBackground(false);
            barChart.setDrawBorders(false);

            // remove axis
            YAxis leftAxis = barChart.getAxisLeft();
            leftAxis.setAxisMinimum(0);
            leftAxis.setEnabled(true);
            YAxis rightAxis = barChart.getAxisRight();
            rightAxis.setEnabled(false);
            XAxis xAxis = barChart.getXAxis();
            xAxis.setEnabled(false);
            BARENTRY = new ArrayList<>();
            BarEntryLabels = new ArrayList<>();

            AddValuesToBARENTRY(plan, fact);
            AddValuesToBarEntryLabels();
            Bardataset = new BarDataSet(BARENTRY, "plan / fact");
            BARDATA = new BarData(Bardataset);
            Bardataset.setColors(ColorTemplate.MATERIAL_COLORS);
            barChart.setData(BARDATA);
            barChart.animateY(900);
        }
    }

    public void AddValuesToBARENTRY(Integer plan, Integer fact){
        BARENTRY.add(new BarEntry(2f, plan));
        BARENTRY.add(new BarEntry(3f, fact));
    }

    public void AddValuesToBarEntryLabels(){
        BarEntryLabels.add("Plan");
        BarEntryLabels.add("Fact");
    }

    public void getAllVisits() {
        if (getActivity() != null) {
            ExploreSharedMemory explore = new ExploreSharedMemory();
            mToken = explore.Explore(getActivity(), "TokenData", "token");
            emplId = Integer.parseInt(explore.Explore(getActivity(), "TokenData", "employeeId"));
            OkHttpClient.Builder okHttp = new OkHttpClient.Builder();

            okHttp.addInterceptor(chain -> {
                Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + mToken).build();
                return chain.proceed(request);
            });

            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl("http://cordialtest.dyndns.org:12015")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttp.build());

            Retrofit retrofit = builder.build();

            final Repository repository = retrofit.create(Repository.class);

            String currentMonthBegin = "";
            String currentMonthEnd = "";
            Date today = new Date();

            Calendar cal = Calendar.getInstance();

            cal.setTime(today);

            currentMonthBegin += cal.get(Calendar.YEAR) + "-";
            currentMonthEnd += cal.get(Calendar.YEAR) + "-";

            currentMonthBegin += (cal.get(Calendar.MONTH) + 1) + "-";
            currentMonthEnd += (cal.get(Calendar.MONTH) + 1) + "-";

            CalendarCalculator calc = new CalendarCalculator();
            currentMonthBegin += "1";
            currentMonthEnd += calc.getMaxDateOfMonth(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));


            Call<List<CollectorVisit>> call = repository.getCollectorVisits(currentMonthBegin, currentMonthEnd, emplId);
            call.enqueue(new Callback<List<CollectorVisit>>() {
                @Override
                public void onResponse(Call<List<CollectorVisit>> call, Response<List<CollectorVisit>> response) {
                    if (response.isSuccessful() && getActivity() != null) {
                        allVisits = response.body();
                        if (allVisits.size() > 0) {
                            for (int i = 0; i < allVisits.size(); i++) {
                                if (allVisits.get(i).getSumm() != null) {
                                    totalSumm += allVisits.get(i).getSumm();
                                }
                                if (allVisits.get(i).getToCashbox() != null) {
                                    totalCashboxSumm += allVisits.get(i).getToCashbox();
                                }
                                currency = allVisits.get(i).getCurrency();
                            }
                        }

                        TextView number1 = getActivity().findViewById(R.id.number_text);
                        TextView curr1 = getActivity().findViewById(R.id.currency_text);
                        TextView number2 = getActivity().findViewById(R.id.number2_text);
                        TextView curr2 = getActivity().findViewById(R.id.currency2_text);

                        number1.setText(totalSumm + "");
                        curr1.setText(currency + "");
                        number2.setText(totalCashboxSumm + "");
                        curr2.setText(currency + "");

                    } else {
                        if (getActivity() != null) {
                            Toasty.warning(getActivity(),
                                    "Нет визитов",
                                    Toast.LENGTH_SHORT,
                                    true).show();
                        }
                    }
                }
                @Override
                public void onFailure(Call<List<CollectorVisit>> call, Throwable t) {
                    if (getActivity() != null) {
                        Toast.makeText(getActivity(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}