package com.collector.kanatbek.erp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Branch {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("branchName")
    @Expose
    private String branchName;

    @SerializedName("branchType")
    @Expose
    private BranchType branchType;


    public Branch(Integer id, String branchName, BranchType branchType) {
        this.id = id;
        this.branchName = branchName;
        this.branchType = branchType;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public BranchType getBranchType() {
        return branchType;
    }

    public void setBranchType(BranchType branchType) {
        this.branchType = branchType;
    }
}
