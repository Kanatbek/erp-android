package com.collector.kanatbek.erp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseExample {
    @SerializedName("_embedded")
    @Expose
    public ContractListEmbedded embedded;
}
