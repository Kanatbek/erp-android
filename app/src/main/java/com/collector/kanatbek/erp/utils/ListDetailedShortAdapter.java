package com.collector.kanatbek.erp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.collector.kanatbek.erp.R;
import com.collector.kanatbek.erp.models.SmService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ListDetailedShortAdapter  extends ArrayAdapter<String> {
    private Activity context;

    private ArrayList<ArrayList<String>> detailedList;
    private SmService smService;

    public ListDetailedShortAdapter(Activity context, List<String> filters_type, ArrayList<ArrayList<String>> detailedList, SmService smService) {
        super(context, R.layout.list_detailed_short_adapter, filters_type);

        this.context = context;
        this.detailedList = detailedList;
        this.smService = smService;
    }

    @NonNull
    @SuppressLint("SetTextI18n")
    public View getView(final int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        @SuppressLint({"ViewHolder", "InflateParams"}) final View rowView = inflater.inflate(R.layout.
                list_detailed_short_adapter, null, true);

        LinearLayout linearLayout = rowView.findViewById(R.id.activityQrDetailedId);
        View vi = inflater.inflate(R.layout.activity_qr_detailed, null);
        TextView tx = vi.findViewById(R.id.btn_finish);

        TextView filterView = rowView.findViewById(R.id.filter_text);
        TextView nextDateView = rowView.findViewById(R.id.nextDate);
        Switch change = rowView.findViewById(R.id.btn1);

        if (detailedList.get(position) != null && detailedList.get(position).size() > 3) {
            boolean isNull = false;
            for (int i=0;i<4;i++) {
                if (detailedList.get(position).get(i) == null) {
                    isNull = true;
                }
            }
            if (!isNull) {
                filterView.setText(detailedList.get(position).get(0) + "");
                nextDateView.setText(detailedList.get(position).get(2) + "");
                Date today = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(today);
                int currentYear = cal.get(Calendar.YEAR);
                String year = detailedList.get(position).get(3).split("-")[0] + "";
                String month = detailedList.get(position).get(3).split("-")[1] + "";
                int intYear = Integer.parseInt(year);
                int intMonth = Integer.parseInt(month);
                intMonth += intYear * 12;
                int currentMonth = currentYear * 12 + (cal.get(Calendar.MONTH) + 1);
                if (currentMonth < intMonth) {
                    nextDateView.setTextColor(getContext().getResources().getColor(R.color.green));
                } else if (currentMonth > intMonth) {
                    nextDateView.setTextColor(getContext().getResources().getColor(R.color.red));
                }
            }

            change.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    if (position == 0) {
                        smService.setPr(1);
                        smService.setPrCounter(1);
//                        tx.setTextColor(rowView.getResources().getColor(R.color.black));
//                        tx.setClickable(true);
//                        linearLayout.addView(vi);
                    } else if (position == 1) {
                        smService.setFno1(1);
                        smService.setFno1Counter(1);
//                        tx.setTextColor(rowView.getResources().getColor(R.color.black));
//                        tx.setClickable(true);
//                        linearLayout.addView(vi);
                    } else if (position == 2) {
                        smService.setFno2(1);
                        smService.setFno2Counter(1);
//                        tx.setTextColor(rowView.getResources().getColor(R.color.black));
//                        tx.setClickable(true);
//                        linearLayout.addView(vi);
                    } else if (position == 3) {
                        smService.setFno3(1);
                        smService.setFno3Counter(1);
//                        tx.setTextColor(rowView.getResources().getColor(R.color.black));
//                        tx.setClickable(true);
//                        linearLayout.addView(vi);
                    } else if(position == 4) {
                        smService.setFno4(1);
                        smService.setFno4Counter(1);
//                        tx.setTextColor(rowView.getResources().getColor(R.color.black));
//                        tx.setClickable(true);
//                        linearLayout.addView(vi);
                    } else {
                        smService.setFno5(1);
                        smService.setFno5Counter(1);
//                        tx.setTextColor(rowView.getResources().getColor(R.color.black));
//                        tx.setClickable(true);
//                        linearLayout.addView(vi);
                    }
                } else {
                    if (position == 0) {
                            smService.setPr(0);
                            smService.setPrCounter(0);
                        } else if (position == 1) {
                            smService.setFno1(0);
                            smService.setFno1Counter(0);
                        } else if (position == 2) {
                            smService.setFno2(0);
                            smService.setFno2Counter(0);
                        } else if (position == 3) {
                            smService.setFno3(0);
                            smService.setFno3Counter(0);
                        } else if(position == 4) {
                            smService.setFno4(0);
                            smService.setFno4Counter(0);
                        } else {
                            smService.setFno5(0);
                            smService.setFno5Counter(0);
                        }
                }
            });
        }
        return rowView;
    }

}
