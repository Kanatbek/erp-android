package com.collector.kanatbek.erp;

import android.Manifest;
import android.annotation.SuppressLint;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.collector.kanatbek.erp.pages.Payment.ContractListByIin;
import com.collector.kanatbek.erp.pages.Profile.ProfileFragment;
import com.collector.kanatbek.erp.pages.QR.FilterChangeDetailed;
import com.collector.kanatbek.erp.models.CollectorPlan;
import com.collector.kanatbek.erp.pages.Plan.PlanTablayout;
import com.collector.kanatbek.erp.pages.QR.QrTablayout;
import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import com.collector.kanatbek.erp.models.Employee;
import com.collector.kanatbek.erp.models.FilterChangeReport;
import com.collector.kanatbek.erp.models.PlanFact;
import com.collector.kanatbek.erp.pages.List.ListTablayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainFragment extends AppCompatActivity {

    private String mToken = "";
    private int companyId = 0;
    private int localBranchId = 0;
    private int partyId = 0;
    private boolean isNew = true;
    private ProgressBar progressBar;
    private Integer CAMERA_PERMISSION_CODE = 1;
    private String loading = "1";
    private Fragment selectedFragment = null;
    BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_fragment);
        progressBar = findViewById(R.id.progressBar);
        loading = "0";

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        Intent intent = getIntent();

        if (intent.getStringExtra("fragment") != null && intent.getStringExtra("fragment").equals("qr")) {
            progressBar.setVisibility(View.INVISIBLE);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new QrTablayout()).commit();
            isNew = false;
        }
        checkingToken();
        getEmployeeByParty();
    }

    public void getPlanCaremanFact() {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        mToken = explore.Explore(this, "TokenData", "token");
        companyId = Integer.parseInt(explore.Explore(this, "TokenData", "companyId"));
        int branchId = Integer.parseInt(explore.Explore(this, "TokenData", "branchId"));
        partyId = Integer.parseInt(explore.Explore(this, "TokenData", "partyId"));
        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();

        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + mToken).build();
            return chain.proceed(request);
        });
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());
        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        String mCurrentTime;
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);

        int currentYear = cal.get(Calendar.YEAR);
        int currentMonth = cal.get(Calendar.MONTH);
        int currentDay = cal.get(Calendar.DAY_OF_MONTH);

        mCurrentTime = currentYear + "-" + (currentMonth + 1) + "-" + currentDay;
        int sendBranchId;
        if (localBranchId != 0) {
            sendBranchId = localBranchId;
        } else {
            sendBranchId = branchId;
        }
        Call<List<PlanFact>> call = repository.getCaremanPlan(companyId, sendBranchId,
                partyId, mCurrentTime);
        PrefLoading("loading");
        call.enqueue(new Callback<List<PlanFact>>() {
            @Override
            public void onResponse(Call<List<PlanFact>> call, Response<List<PlanFact>> response) {
                if (response.isSuccessful()) {
                    SharedPreferences.Editor editPreference = getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
                    List<PlanFact> planFact = response.body();
                    if (planFact.size() > 0) {
                        if (planFact.get(0).getPlan() != null) {
                            int plan = planFact.get(0).getPlan();
                            editPreference.putString("plan_careman", plan + "");
                            editPreference.apply();
                        }
                        if (planFact.get(0).getFact() != null) {
                            int fact = planFact.get(0).getFact();
                            editPreference.putString("fact_careman", fact + "");
                            editPreference.apply();
                        }
                        PrefLoading("0");
                    }
                    getCollectorByParty();
                } else {
                    getCollectorByParty();
                }
            }

            @Override
            public void onFailure(Call<List<PlanFact>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
                PrefLoading("0");
            }
        });
    }

    public void getPlanCollectorFact() {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        mToken = explore.Explore(this, "TokenData", "token");
        companyId = Integer.parseInt(explore.Explore(this, "TokenData", "companyId"));
        int branchId = Integer.parseInt(explore.Explore(this, "TokenData", "branchId"));
        partyId = Integer.parseInt(explore.Explore(this, "TokenData", "partyId"));
        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();

        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + mToken).build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();

        final Repository repository = retrofit.create(Repository.class);

        String mCurrentTime;
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);

        int currentYear = cal.get(Calendar.YEAR);
        int currentMonth = cal.get(Calendar.MONTH);
        int currentDay = cal.get(Calendar.DAY_OF_MONTH);

        mCurrentTime = currentYear + "-" + (currentMonth + 1) + "-" + currentDay;
        int sendBranchId;
        if (localBranchId != 0) {
            sendBranchId = localBranchId;
        } else {
            sendBranchId = branchId;
        }
        Call<List<CollectorPlan>> call = repository.getCollectorPlan(companyId, sendBranchId,
                31, partyId, mCurrentTime);
        PrefLoading("loading");
        call.enqueue(new Callback<List<CollectorPlan>>() {
            @Override
            public void onResponse(Call<List<CollectorPlan>> call, Response<List<CollectorPlan>> response) {
                if (response.isSuccessful()) {
                    SharedPreferences.Editor editPreference = getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
                    List<CollectorPlan> planFact = response.body();
                    if (planFact.size() > 0) {
                        if (planFact.get(0).getPlan() != null) {
                            int plan = planFact.get(0).getPlan();
                            editPreference.putString("plan_collector", plan + "");
                            editPreference.apply();
                        }
                        if (planFact.get(0).getFact() != null) {
                            int fact = planFact.get(0).getFact();
                            editPreference.putString("fact_collector", fact + "");
                            editPreference.apply();
                        }
                        PrefLoading("0");
                    }
                } else {

                    if (!isNew) {
                        if (getApplicationContext() != null && getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                            String currenctFragment = explore.Explore(MainFragment.this, "TokenData", "currentFragment");
                            if (!currenctFragment.equals("careman_List")
                                    && !currenctFragment.equals("careman_Profile")
                                    && !currenctFragment.equals("careman_qr")
                                    && !currenctFragment.equals("careman_Code")) {
                                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                        new PlanTablayout()).commit();
                                PrefLoading("0");
                            }
                        }

                    }
                }
                 if (isNew) {
                     if (getApplicationContext() != null && getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                         String currenctFragment = explore.Explore(MainFragment.this, "TokenData", "currentFragment");
                         if (!currenctFragment.equals("careman_List")
                                 && !currenctFragment.equals("careman_Profile")
                                 && !currenctFragment.equals("careman_qr")
                                 && !currenctFragment.equals("careman_Code")) {
                             getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                     new PlanTablayout()).commit();
                             PrefLoading("0");
                         }
                     }
                 }

            }
            @Override
            public void onFailure(Call<List<CollectorPlan>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
                PrefLoading("0");
            }
        });
    }

    public void PrefLoading(String value) {
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor pref =
                        getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
        pref.putString("loading", value);
        loading = value;
    }

    public void getEmployeeByParty() {
        PrefLoading("loading");
        ExploreSharedMemory explore = new ExploreSharedMemory();
        mToken = explore.Explore(this, "TokenData", "token");
        partyId = Integer.parseInt(explore.Explore(this, "TokenData", "partyId"));
        companyId = Integer.parseInt(explore.Explore(this, "TokenData", "companyId"));

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + mToken).build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        Call<Employee> call = repository.getCaremanByParty(partyId, "Employee");
        call.enqueue(new Callback<Employee>() {
            @Override
            public void onResponse(Call<Employee> call, Response<Employee> response) {
                if (response.body() != null) {
                    PrefLoading("0");
                    SharedPreferences.Editor editPreference = getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
                    if (response.body().getCompany() != null) {
                        editPreference.putString("companyName", response.body().getCompany().getName());
                    }
                    if (response.body().getPosition() != null) {
                        editPreference.putString("positionName", response.body().getPosition().getName());
                    }
                    if (response.body().getBranch() != null) {
                        localBranchId = response.body().getBranch().getId();
                        editPreference.putString("caremanBranchId", response.body().getBranch().getId().toString());
                        editPreference.putString("caremanBranchName", response.body().getBranch().getBranchName());
                    }
                    if (response.body().getId() != null) {
                        editPreference.putString("caremanId", response.body().getId() + "");
                    }
                    if (response.body().getParty() != null && response.body().getParty().getPhotoString() != null) {
                        editPreference.putString("photo", response.body().getParty().getPhotoString());
                    }
                    editPreference.apply();
                    getPlanCaremanFact();
                } else {
                    getCollectorByParty();
                }
            }
            @Override
            public void onFailure(Call<Employee> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
                PrefLoading("0");
                getCollectorByParty();
            }
        });
    }

    public void getCollectorByParty() {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        PrefLoading("loading");
        mToken = explore.Explore(this, "TokenData", "token");
        partyId = Integer.parseInt(explore.Explore(this, "TokenData", "partyId"));
        companyId = Integer.parseInt(explore.Explore(this, "TokenData", "companyId"));

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request()
                    .newBuilder()
                    .addHeader("Authorization", "Bearer " + mToken)
                    .build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        Repository repository = retrofit.create(Repository.class);
        Call call = repository.getCollectorByParty(partyId, "Employee");
        call.enqueue(new Callback<Employee>() {
            @Override
            public void onResponse(Call<Employee> call, Response<Employee> response) {
                if (response.isSuccessful() && response.body() != null) {
                    PrefLoading("0");
                    SharedPreferences.Editor editPreference = getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
                    if (response.body().getId() != null) {
                        editPreference.putString("collectorId", response.body().getId() + "");
                    }
                    if (response.body().getBranch() != null) {
                        localBranchId = response.body().getBranch().getId();
                        editPreference.putString("collectorBranchId", response.body().getBranch().getId().toString());
                        editPreference.putString("collectorBranchName", response.body().getBranch().getBranchName());
                    }
                    if (response.body().getCompany() != null) {
                        editPreference.putString("collectorCompanyName", response.body().getCompany().getName());
                    }
                    editPreference.apply();
                    getPlanCollectorFact();
                } else {
                    String currenctFragment = explore.Explore(MainFragment.this, "TokenData", "currentFragment");
                    if (getApplicationContext() != null && getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                        if (!currenctFragment.equals("careman_List")
                                && !currenctFragment.equals("careman_Profile")
                                && !currenctFragment.equals("careman_qr")
                                && !currenctFragment.equals("careman_Code")) {
                            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                    new PlanTablayout()).commit();
                            PrefLoading("0");
                        }

                    }
                }
            }
            @Override
            public void onFailure(Call<Employee> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
                PrefLoading("0");
            }
        });
    }

    public void checkingToken() {
        SharedPreferences preferences = getSharedPreferences("TokenData", Context.MODE_PRIVATE);
        if (preferences.getString("loginDate", null) != null) {
            String loginDateString = preferences.getString("loginDate", null);

            long tokenMillis = Long.valueOf(loginDateString);
            Date currentDate = new Date();
            Calendar c = Calendar.getInstance();
            c.setTime(currentDate);
            long currentMillis = c.getTimeInMillis();

            int tokenDays = (int) ((tokenMillis / 1000) / 3600);
            int currentDays = (int) ((currentMillis / 1000) / 3600);

            if (currentDays - tokenDays > 15) {
                logOut();
            }
        }
    }

    public void logOut() {
        getSharedPreferences("TokenData", Context.MODE_PRIVATE)
                .edit()
                .clear()
                .apply();
        Intent i = new Intent(this, LoginPageActivity.class);
        startActivity(i);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            item -> {
                ExploreSharedMemory explore = new ExploreSharedMemory();
                String currenctFragment = explore.Explore(this, "TokenData", "currentFragment");
                String value = "";
                if (explore.Explore(this, "TokenData", "loading").equals("1")) {
                    value = loading;
                } else {
                    value = explore.Explore(this, "TokenData", "loading");
                }
//                if (value.equals("0")) {
                    switch (item.getItemId()) {
                        case R.id.navigation_home:
                            if (!currenctFragment.equals("careman_plan")) {
                                selectedFragment = new PlanTablayout();
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                            break;
                        case R.id.navigation_dashboard:
                            if (!currenctFragment.equals("careman_List")) {
                                selectedFragment = new ListTablayout();
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                            break;
                        case R.id.navigation_profile:
                            if (!currenctFragment.equals("careman_Profile")) {
                                selectedFragment = new ProfileFragment();
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                            break;
                        case R.id.navigation_qr_code:
                            if (!currenctFragment.equals("careman_Code")) {
                                checkForPast();

                                askForPermission(Manifest.permission.ACCESS_FINE_LOCATION, CAMERA_PERMISSION_CODE);
//                                selectedFragment = new QrTablayout();
                                progressBar.setVisibility(View.INVISIBLE);
                                }
                            break;
                    }
                    if (selectedFragment != null) {
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                selectedFragment).commit();
                    }
//                }
                return true;
            };

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(MainFragment.this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainFragment.this, permission)) {
                        ActivityCompat.requestPermissions(MainFragment.this,
                                                new String[]{permission}, requestCode);

            } else {
                        ActivityCompat.requestPermissions(MainFragment.this,
                                                new String[]{permission}, requestCode);
            }
        } else {
            if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                askForPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
            } else {
                selectedFragment = new QrTablayout();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        selectedFragment).commit();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        askForPermission(Manifest.permission.CAMERA, CAMERA_PERMISSION_CODE);
                    } else {
                        selectedFragment = new QrTablayout();
                        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                selectedFragment).commit();
                    }

                } else {
                    selectedFragment = null;
                    Toasty.error(getApplicationContext(),
                                            "Нужно разрешения на использование GPS",
                                             Toast.LENGTH_LONG,
                                            true).show();
                }
                return;
            }
        }
    }

    public void checkForPast() {
        SharedPreferences preferences = getSharedPreferences("TokenData", Context.MODE_PRIVATE);
        if (preferences.getString("SN", null) != null) {
            String sn = preferences.getString("SN", null);
            if (sn != null && sn.length() > 0) {
                getReportBySN(sn);
            }
        }
        if (preferences.getString("barCode", null) != null) {
            String barCode = preferences.getString("barCode", null);
            if (barCode != null && barCode.length() > 0) {
                Intent intent = new Intent(MainFragment.this, ContractListByIin.class);
                intent.putExtra("iin", barCode);
                startActivity(intent);
            }
        }
    }

    public void getReportBySN(String sr) {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        mToken = explore.Explore(this, "TokenData", "token");

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + mToken).build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        Call<FilterChangeReport> call = repository.getReportBySN(sr);
        call.enqueue(new Callback<FilterChangeReport>() {
            @Override
            public void onResponse(Call<FilterChangeReport> call, Response<FilterChangeReport> response) {
                Intent i = new Intent(MainFragment.this, FilterChangeDetailed.class);
                if (response.body() != null) {
                    i.putExtra("contractNumber", response.body().getContractNumber());
                    i.putExtra("sdate", response.body().getSdate());
                    i.putExtra("clientFio", response.body().getExploiterFio());
                    i.putExtra("serialNumber", response.body().getSerialNumber());
                    i.putExtra("caremanFio", response.body().getCaremanFio());
                    i.putExtra("customerId", String.valueOf(response.body().getCustomerId()));
                    i.putExtra("paymentSumm", ((response.body().getPayment() != null) ?
                            response.body().getPayment().toString() : null));
                    i.putExtra("iin", String.valueOf(response.body().getCustomerIin()));
                    i.putExtra("currency", response.body().getCurrency());

                    i.putExtra("f1MT", String.valueOf(response.body().getF1Mt()));
                    i.putExtra("f2MT", String.valueOf(response.body().getF2Mt()));
                    i.putExtra("f3MT", String.valueOf(response.body().getF3Mt()));
                    i.putExtra("f4MT", String.valueOf(response.body().getF4Mt()));
                    i.putExtra("f5MT", String.valueOf(response.body().getF5Mt()));
                    i.putExtra("prMT", String.valueOf(response.body().getPrMt()));

                    i.putExtra("f1Last", response.body().getF1Last());
                    i.putExtra("f1Next", response.body().getF1Next());

                    i.putExtra("f2Last", response.body().getF2Last());
                    i.putExtra("f2Next", response.body().getF2Next());

                    i.putExtra("f3Last", response.body().getF3Last());
                    i.putExtra("f3Next", response.body().getF3Next());

                    i.putExtra("f4Last", response.body().getF4Last());
                    i.putExtra("f4Next", response.body().getF4Next());

                    i.putExtra("f5Last", response.body().getF5Last());
                    i.putExtra("f5Next", response.body().getF5Next());

                    i.putExtra("prLast", response.body().getPrLast());
                    i.putExtra("prNext", response.body().getPrNext());

                    i.putExtra("address", response.body().getAddress());
                    i.putExtra("phones", response.body().getPhone());
                    startActivity(i);
                }
            }
            @Override
            public void onFailure(Call<FilterChangeReport> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }


}
