package com.collector.kanatbek.erp.pages.Payment;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.collector.kanatbek.erp.models.Contract;
import com.collector.kanatbek.erp.R;
import java.util.List;

public class ContractRecycleAdapter extends RecyclerView.Adapter<ContractRecycleAdapter.ViewHolder> {

    private List<Contract> contracts;
    private LayoutInflater mInflater;

    private List<String> contractTitle;
    private OnItemClickListener onItemClickListener;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView contractName, contractNumber,
                        contractSrok, contractCost,
                        contractPaid, contractPayment;

        public ViewHolder(View view) {
            super(view);
            contractName = view.findViewById(R.id.contract_name_id);
            contractNumber = view.findViewById(R.id.contract_number_id);
            contractSrok = view.findViewById(R.id.contract_srok_id);
            contractCost = view.findViewById(R.id.contract_cost_id);
            contractPaid = view.findViewById(R.id.contract_paid_id);
            contractPayment = view.findViewById(R.id.contract_payment_id);
        }

        public void bind(Contract contract, OnItemClickListener listener) {
            itemView.setOnClickListener(v -> {
                listener.onItemClickListener(contract);
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClickListener(Contract item);
    }

    public ContractRecycleAdapter(Activity context, List<String> contractTitle, List<Contract> contracts, OnItemClickListener onItemClickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.contractTitle = contractTitle;
        this.contracts = contracts;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.list_contract, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {

        if (i % 2 == 0) {
            holder.itemView.setBackgroundColor(holder.itemView.getResources().getColor(R.color.blurbackground));
        } else {
            holder.itemView.setBackgroundColor(holder.itemView.getResources().getColor(R.color.white));
        }
        int cost = contracts.get(i).getCost() != null ? contracts.get(i).getCost() : 0;
        int paid = contracts.get(i).getPaid() != null ? contracts.get(i).getPaid() : 0;
        String name = contracts.get(i).getInventory() != null ?
                contracts.get(i).getInventory().getName() : "Товар";

        holder.contractName.setText(name);
        holder.contractNumber.setText(contracts.get(i).getContractNumber() + "");
        holder.contractSrok.setText(contracts.get(i).getMonth() + "");
        holder.contractCost.setText(cost + "");
        holder.contractPaid.setText(paid + "");
        holder.contractPayment.setText((cost - paid) + "");
        holder.bind(contracts.get(i), onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return contracts.size();
    }
}
