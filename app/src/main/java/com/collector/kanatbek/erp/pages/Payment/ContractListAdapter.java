package com.collector.kanatbek.erp.pages.Payment;

import android.annotation.SuppressLint;
import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.collector.kanatbek.erp.R;
import java.util.List;

public class ContractListAdapter extends ArrayAdapter<String> {

    private Activity context;
    private List<String> contractNumbers;
    private List<Integer> contractSroks;
    private List<Integer> contractCosts;
    private List<Integer> contractPaids;
    private List<Integer> contractPayments;
    private String currency = "";
    private String contractNames = "";

    public ContractListAdapter(Activity context, List<String> contractNumber,
                               List<Integer> contractSrok, List<Integer> contractCost,
                               List<Integer> contractPaid, List<Integer> contractPayment,
                               String currency, String contractName) {
        super(context, R.layout.list_contract, contractNumber);
        this.context = context;
        this.contractNumbers = contractNumber;
        this.contractSroks = contractSrok;
        this.contractCosts = contractCost;
        this.contractPaids = contractPaid;
        this.contractPayments = contractPayment;
        this.currency = currency;
        this.contractNames = contractName;
    }

    @NonNull
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        @SuppressLint("ViewHolder") View rowView = inflater.inflate(R.layout.list_contract, null, true);

        TextView contractNumber = rowView.findViewById(R.id.contract_number_id);
        TextView contractSrok = rowView.findViewById(R.id.contract_srok_id);
        TextView contractCost = rowView.findViewById(R.id.contract_cost_id);
        TextView contractPaid  = rowView.findViewById(R.id.contract_paid_id);
        TextView contractPayment = rowView.findViewById(R.id.contract_payment_id);
        TextView contractName = rowView.findViewById(R.id.contract_name_id);

        contractNumber.setText(contractNumbers.get(position) + "");
        contractSrok.setText(contractSroks.get(position) + " месяц");
        contractCost.setText(contractCosts.get(position) + " " + currency);
        contractPaid.setText(contractPaids.get(position) + " " + currency);
        contractPayment.setText(contractPayments.get(position) + " " + currency);
        contractName.setText(contractNames + "");

        return rowView;
    }


}
