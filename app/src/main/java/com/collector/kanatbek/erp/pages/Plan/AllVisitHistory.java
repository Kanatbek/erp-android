package com.collector.kanatbek.erp.pages.Plan;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.collector.kanatbek.erp.utils.CalendarCalculator;
import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import com.collector.kanatbek.erp.models.CollectorVisit;
import com.collector.kanatbek.erp.Repository;
import com.collector.kanatbek.erp.utils.SmVisitHistoryAdapter;
import com.collector.kanatbek.erp.R;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AllVisitHistory extends AppCompatActivity {

    private List<CollectorVisit> allVisits;
    private String mToken = "";
    private Integer emplId = 0;
    private Integer partyId = 0;
    private SmVisitHistoryAdapter adapter;
    private Call<List<CollectorVisit>> call;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.all_visit_history);
        Intent intent = getIntent();
        String pi = intent.getStringExtra("partyId");
        if (pi != null) {
            partyId = Integer.parseInt(pi);
        }

        getVisitHistory();
    }

    public void getVisitHistory() {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        mToken = explore.Explore(this, "TokenData", "token");
        emplId = Integer.parseInt(explore.Explore(this, "TokenData", "employeeId"));
        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();

        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + mToken).build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();

        final Repository repository = retrofit.create(Repository.class);

        String currentMonthBegin = "";
        String currentMonthEnd = "";
        Date today = new Date();

        Calendar cal = Calendar.getInstance();

        cal.setTime(today);

        currentMonthBegin += cal.get(Calendar.YEAR) + "-";
        currentMonthEnd += cal.get(Calendar.YEAR) + "-";

        currentMonthBegin += (cal.get(Calendar.MONTH) + 1) + "-";
        currentMonthEnd += (cal.get(Calendar.MONTH) + 1) + "-";

        CalendarCalculator calc = new CalendarCalculator();
        currentMonthBegin += "1";
        currentMonthEnd += calc.getMaxDateOfMonth(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));

        if (partyId != 0) {
            call = repository.getCollectorVisitsByCustomer(currentMonthBegin, currentMonthEnd, emplId, partyId);
        } else {
            call = repository.getCollectorVisits(currentMonthBegin, currentMonthEnd, emplId);
        }

        call.enqueue(new Callback<List<CollectorVisit>>() {
            @Override
            public void onResponse(Call<List<CollectorVisit>> call, Response<List<CollectorVisit>> response) {
                if (response.isSuccessful()
                        && response.body().size() > 0) {
                    allVisits = response.body();
                    fillVisitList();
                } else {
                    Toasty.info(getApplicationContext(),
                            "Нет визитов",
                            Toast.LENGTH_SHORT,
                            true).show();
                }
            }
            @Override
            public void onFailure(Call<List<CollectorVisit>> call, Throwable t) {
                if (getApplicationContext() != null) {
                    Toast.makeText(getApplicationContext(), t.getMessage() + "", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void fillVisitList() {
        ListView listView = findViewById(R.id.visit_history);
        adapter = new SmVisitHistoryAdapter(this, allVisits, partyId);
        listView.setAdapter(adapter);
        TextView txt1 = findViewById(R.id.txt1);
        TextView txt2 = findViewById(R.id.txt2);
        TextView txt3 = findViewById(R.id.txt3);
        txt1.setText("Дата");
        txt3.setText("Время сервиса");
        if (partyId == 0) {
            txt2.setText("Фио клиента");
        } else {
            txt2.setText("Фио взносщика");
        }

        listView.setOnItemClickListener((parent, view, position, id) -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(AllVisitHistory.this);
            builder.setMessage("Сумма у взносщика: "
                    + (allVisits.get(position).getSumm() != null ? allVisits.get(position).getSumm() : 0) + " "
                    + ((allVisits.get(position).getCurrency() != null ? allVisits.get(position).getCurrency() : "") + "\n")
                    + "Сумма в кассе: "
                    + (allVisits.get(position).getToCashbox() != null ? allVisits.get(position).getToCashbox() : 0) + " "
                    + ((allVisits.get(position).getCurrency() != null ? allVisits.get(position).getCurrency() : "") + "\n")
                    + "Инфо: "
                    + (allVisits.get(position).getInfo() != null ? allVisits.get(position).getInfo() : "нет заметок") );

            builder.setCancelable(false);

            builder.setPositiveButton("OK", (dialog, which) -> {

            });
            AlertDialog alert = builder.create();
            alert.show();
        });
    }
}


