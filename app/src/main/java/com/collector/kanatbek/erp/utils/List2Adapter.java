package com.collector.kanatbek.erp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.collector.kanatbek.erp.R;
import java.util.List;

public class List2Adapter extends ArrayAdapter<String> {

    private Activity context;
    private List<String> arraySet;
    private List<String> arrayText;

    public List2Adapter(Activity context, List<String> arraySet, List<String> arrayText) {
        super(context, R.layout.list_2, arraySet);

        this.context = context;
        this.arraySet = arraySet;
        this.arrayText = arrayText;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        @SuppressLint({"ViewHolder", "InflateParams"}) View rowView = inflater.inflate(R.layout.list_2, null, true);

        if (position % 2 == 0) {
            rowView.setBackgroundColor(getContext().getResources().getColor(R.color.blurbackground));
        }
        TextView firstText = rowView.findViewById(R.id.first_text);
        TextView secondText = rowView.findViewById(R.id.second_text);

        if (arraySet.get(position) != null) {
            firstText.setText(arraySet.get(position) + "");
        }
        if (arrayText.get(position) != null) {
            secondText.setText(arrayText.get(position) + "");
        }

        return rowView;
    }
}
