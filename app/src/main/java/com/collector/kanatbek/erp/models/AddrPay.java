package com.collector.kanatbek.erp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddrPay {
    @SerializedName("id")
    @Expose
    private  Integer id;

    @SerializedName("district")
    @Expose
    private String district;

    @SerializedName("street")
    @Expose
    private String street;

    @SerializedName("houseNumber")
    @Expose
    private String houseNumber;

    @SerializedName("flatNumber")
    @Expose
    private String flatNumber;

    @SerializedName("zipCode")
    @Expose
    private String zipCode;

    @SerializedName("info")
    @Expose
    private String info;

    public AddrPay(Integer id) {
        this.id = id;
    }

    public AddrPay(Integer id, String district, String street, String houseNumber, String flatNumber, String zipCode, String info) {
        this.id = id;
        this.district = district;
        this.street = street;
        this.houseNumber = houseNumber;
        this.flatNumber = flatNumber;
        this.zipCode = zipCode;
        this.info = info;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
