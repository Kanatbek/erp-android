package com.collector.kanatbek.erp.models;

public class SmServiceList {
    private Integer contractId;
    private String customerFIO;
    private String currency;
    private Integer paymentDue;

    public SmServiceList() {

    }

    public SmServiceList(Integer contractId, String customerFIO,
                         String currency, Integer paymentDue) {
        this.contractId = contractId;
        this.customerFIO = customerFIO;
        this.currency = currency;
        this.paymentDue = paymentDue;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getCustomerFIO() {
        return customerFIO;
    }

    public void setCustomerFIO(String customerFIO) {
        this.customerFIO = customerFIO;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getPaymentDue() {
        return paymentDue;
    }

    public void setPaymentDue(Integer paymentDue) {
        this.paymentDue = paymentDue;
    }

}
