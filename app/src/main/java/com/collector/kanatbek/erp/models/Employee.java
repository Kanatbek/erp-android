package com.collector.kanatbek.erp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Employee {
    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("company")
    @Expose
    private Company company;

    @SerializedName("branch")
    @Expose
    private Branch branch;

    @SerializedName("position")
    @Expose
    private Position position;

    @SerializedName("party")
    @Expose
    private Party party;

    public Employee () {

    }

    public Employee(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Party getParty() { return party; }

    public void setParty(Party party) { this.party = party; }
}
