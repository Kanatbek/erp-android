package com.collector.kanatbek.erp.pages.QR;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.collector.kanatbek.erp.models.FilterChangeReport;
import com.collector.kanatbek.erp.R;
import com.collector.kanatbek.erp.Repository;
import com.collector.kanatbek.erp.utils.ExploreSharedMemory;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import im.delight.android.location.SimpleLocation;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class QrBySn extends Fragment {

    private Button continueBtn;
    private ImageView qrImg;
    private EditText qrText;
    private String mToken = "";
    private SimpleLocation location;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_qr_iin, container, false);

        if (getActivity() != null) {
            location = new SimpleLocation(getActivity());
            SharedPreferences.Editor pref = getActivity().getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
            pref.putString("currentFragment", "careman_qr");
            pref.apply();
        }

        continueBtn = view.findViewById(R.id.continueBtn);
        qrImg = view.findViewById(R.id.img1);
        qrText = view.findViewById(R.id.snText);

        qrImg.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), QrCareman.class);
            startActivityForResult(intent, 1);
        });
        continueBtn.setOnClickListener(v -> {
            if (qrText.getText().length() > 5) {
                getReportBySN(qrText.getText().toString());
                saveTimeAndSN(qrText.getText().toString());
                saveLocation();
            } else {
                Toasty.warning(getActivity(),
                        "Заполните поле",
                        Toast.LENGTH_SHORT,
                        true).show();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (location != null) {
            location.beginUpdates();
        }
    }

    @Override
    public void onPause() {
        if (location != null) {
            location.endUpdates();
        }
        super.onPause();
    }

    public void getReportBySN(String sr) {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        mToken = explore.Explore(getActivity(), "TokenData", "token");

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + mToken).build();
            return chain.proceed(request);
        });
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        Call<FilterChangeReport> call = repository.getReportBySN(sr);
        call.enqueue(new Callback<FilterChangeReport>() {
            @Override
            public void onResponse(Call<FilterChangeReport> call, Response<FilterChangeReport> response) {
                if (getActivity() != null) {
                    Intent i = new Intent(getActivity(), FilterChangeDetailed.class);
                    if (response.body() != null) {
                        i.putExtra("contractNumber", response.body().getContractNumber());
                        i.putExtra("sdate", response.body().getSdate());
                        i.putExtra("clientFio", response.body().getExploiterFio());
                        i.putExtra("serialNumber", response.body().getSerialNumber());
                        i.putExtra("caremanFio", response.body().getCaremanFio());
                        i.putExtra("customerId", String.valueOf(response.body().getCustomerId()));
                        i.putExtra("iin", response.body().getCustomerIin().toString());
                        i.putExtra("paymentSumm", ((response.body().getPayment() != null)
                                ? response.body().getPayment().toString() : null));
                        i.putExtra("currency", response.body().getCurrency());

                        i.putExtra("f1MT", String.valueOf(response.body().getF1Mt()));
                        i.putExtra("f2MT", String.valueOf(response.body().getF2Mt()));
                        i.putExtra("f3MT", String.valueOf(response.body().getF3Mt()));
                        i.putExtra("f4MT", String.valueOf(response.body().getF4Mt()));
                        i.putExtra("f5MT", String.valueOf(response.body().getF5Mt()));
                        i.putExtra("prMT", String.valueOf(response.body().getPrMt()));

                        i.putExtra("f1Last", response.body().getF1Last());
                        i.putExtra("f1Next", response.body().getF1Next());

                        i.putExtra("f2Last", response.body().getF2Last());
                        i.putExtra("f2Next", response.body().getF2Next());

                        i.putExtra("f3Last", response.body().getF3Last());
                        i.putExtra("f3Next", response.body().getF3Next());

                        i.putExtra("f4Last", response.body().getF4Last());
                        i.putExtra("f4Next", response.body().getF4Next());

                        i.putExtra("f5Last", response.body().getF5Last());
                        i.putExtra("f5Next", response.body().getF5Next());

                        i.putExtra("prLast", response.body().getPrLast());
                        i.putExtra("prNext", response.body().getPrNext());

                        i.putExtra("address", response.body().getAddress());
                        i.putExtra("phones", response.body().getPhone());
                        startActivity(i);
                    } else {
                        Toasty.warning(getActivity(),
                                "Не найдено график замены фильтров",
                                Toast.LENGTH_SHORT,
                                true).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<FilterChangeReport> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void saveTimeAndSN(String sn) {
        String mCurrentTime;
        String mCurrentHour;
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);

        mCurrentTime = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) +1) + "-" + cal.get(Calendar.DAY_OF_MONTH);
        mCurrentHour = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);

        SharedPreferences.Editor preference = Objects.requireNonNull(getActivity())
                .getSharedPreferences("TokenData",
                        Context.MODE_PRIVATE).edit();
        preference.putString("start_day", mCurrentTime);
        preference.putString("start_time", mCurrentHour);
        preference.putString("SN", sn);
        preference.apply();
        preference.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        String code = data.getStringExtra("qrCode");
        if (code != null && code.length() > 5){
            if (requestCode == 1) {
                qrText.setText(code);
            }
        }
    }

    public void saveLocation() {
        if (getActivity() != null) {
            if (!location.hasLocationEnabled()) {
                SimpleLocation.openSettings(getActivity());
            }
            SharedPreferences.Editor pref = getActivity().getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
            pref.putString("longitude", location.getLongitude() + "");
            pref.putString("latitude", location.getLatitude() + "");
            pref.apply();
        }

    }
}
