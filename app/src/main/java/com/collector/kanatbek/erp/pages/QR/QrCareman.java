package com.collector.kanatbek.erp.pages.QR;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.collector.kanatbek.erp.R;
import java.util.Objects;

public class QrCareman extends Activity {

    private CodeScanner mCodeScanner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_qr_code);
        if (getApplicationContext() != null) {
            SharedPreferences.Editor pref = getApplicationContext().getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
            pref.putString("currentFragment", "careman_Code");
            pref.apply();
        }

        CodeScannerView scannerView = findViewById(R.id.scanner_view);

        mCodeScanner = new CodeScanner(getApplicationContext(), scannerView);
        mCodeScanner.setDecodeCallback(result -> {
            if (getApplicationContext() != null) {
                Objects.requireNonNull(this).runOnUiThread(() -> {
                    mCodeScanner.stopPreview();
                    if (result.getText() != null) {
                        if (getApplicationContext() != null) {
                            Intent intent = new Intent();
                            intent.putExtra("qrCode", result.getText());
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                });
            }
        });

        scannerView.setOnClickListener(view -> mCodeScanner.startPreview());
    }

    @Override
    public void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    public void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }
}