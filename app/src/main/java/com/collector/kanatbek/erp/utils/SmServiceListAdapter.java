package com.collector.kanatbek.erp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.collector.kanatbek.erp.R;
import java.util.List;

public class SmServiceListAdapter extends ArrayAdapter<String> {

    private Activity context;

    private List<Integer> paymentDue;

    private List<String> customerFio;

    private List<String> currency;

    public SmServiceListAdapter(Activity context, List<String> customerFio, List<Integer> paymentDue, List<String> currency) {
        super(context, R.layout.sm_service_lists, customerFio);

        this.context = context;
        this.customerFio = customerFio;
        this.paymentDue = paymentDue;
        this.currency = currency;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        @SuppressLint({"ViewHolder", "InflateParams"}) View rowView = inflater.inflate(R.layout.sm_service_lists, null, true);
        if (position % 2 == 1) {
            rowView.setBackgroundColor(rowView.getResources().getColor(R.color.gray));
        } else {
            rowView.setBackgroundColor(rowView.getResources().getColor(R.color.white));
        }

        TextView id = rowView.findViewById(R.id.textview_id);
        TextView customer = rowView.findViewById(R.id.textview_conDate);
        TextView fact = rowView.findViewById(R.id.textview_fact);

        if (customerFio != null && customerFio.size() > 0) {
            for (int i=0;i<customerFio.size();i++) {
                id.setText("# " + (position + 1));
                customer.setText(customerFio.get(position));

                String paid = "";
                if (paymentDue.get(position) != null) {
                    paid = paymentDue.get(position) + " ";
                }
                if (currency.get(position) != null) {
                    paid += currency.get(position);
                }
                fact.setText(paid);
            }
        }
        return rowView;
    }
}