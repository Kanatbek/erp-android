package com.collector.kanatbek.erp.pages.List;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.collector.kanatbek.erp.utils.CalendarCalculator;
import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import com.collector.kanatbek.erp.R;
import com.collector.kanatbek.erp.models.FilterChangeReport;
import com.collector.kanatbek.erp.Repository;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListCaremanFragment extends Fragment {

    String token = "";
    Integer companyId = 0;
    Integer branchId = 0;
    Integer serviceBranchId = 0;
    Integer employeeId = 0;
    private View view;

    private SpinKitView spinKitView;

    ListView listView;
    public List<String> careman = new ArrayList<>();
    public ArrayList<ArrayList<Integer>> numbers = new ArrayList<>();

    EditText searchText;
    SharedPreferences.Editor preferences;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        if (getActivity() != null) {
            preferences = getActivity().getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();

            spinKitView = view.findViewById(R.id.spin_kitt);
            spinKitView.setVisibility(View.VISIBLE);
            if (numbers != null && numbers.size() > 0) {
                //
            } else {
                fillRealList();
            }
            setRetainInstance(true);
        }
        return view;
    }

    public void fillRealList() {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        PrefLoading("loading");
        String currentMonthBegin = "";
        String currentMonthEnd = "";
        Date today = new Date();

        Calendar cal = Calendar.getInstance();

        cal.setTime(today);

        currentMonthBegin += cal.get(Calendar.YEAR) + "-";
        currentMonthEnd += cal.get(Calendar.YEAR) + "-";

        currentMonthBegin += (cal.get(Calendar.MONTH) + 1) + "-";
        currentMonthEnd += (cal.get(Calendar.MONTH) + 1) + "-";

        CalendarCalculator calc = new CalendarCalculator();
        currentMonthBegin += "1";
        currentMonthEnd += calc.getMaxDateOfMonth(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH));

        token = explore.Explore(getActivity(), "TokenData", "token");
        companyId = Integer.parseInt(explore.Explore(getActivity(), "TokenData", "companyId"));
        branchId = Integer.parseInt(explore.Explore(getActivity(), "TokenData", "caremanBranchId"));
        serviceBranchId = Integer.parseInt(explore.Explore(getActivity(), "TokenData", "serviceBranchId"));
        employeeId = Integer.parseInt(explore.Explore(getActivity(), "TokenData", "caremanId"));

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();

        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + token).build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());
        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        Call<List<FilterChangeReport>> call = repository.getCaremanReport(companyId, branchId,
                                                false, currentMonthBegin,
                                                currentMonthEnd, employeeId);
        call.enqueue(new Callback<List<FilterChangeReport>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<FilterChangeReport>> call, Response<List<FilterChangeReport>> response) {
                if (response.isSuccessful()) {
                    spinKitView.setVisibility(View.INVISIBLE);
                    List<FilterChangeReport> reports = response.body();
                    if (reports.size() > 0) {
                        setIdForAll(reports);
                        extractReports(reports);
                        PrefLoading("0");
                    }
                } else {
                    if (getContext() != null) {
                        Toasty.error(getContext(),
                                "Проблема с интернетом",
                                Toast.LENGTH_SHORT,
                                true).show();
                    }
                    PrefLoading("0");
                }
            }
            @Override
            public void onFailure(Call<List<FilterChangeReport>> call, Throwable t) {
                PrefLoading("0");
            }
        });
    }

    public void PrefLoading(String loading) {
        preferences.putString("loading", loading);
        preferences.apply();
    }

    public void setIdForAll(List<FilterChangeReport> report) {
            for (int i=0; i<report.size();i++) {
                report.get(i).setId(i+1);
            }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void extractReports(final List<FilterChangeReport> repo) {
        numbers = new ArrayList<>();
        for (int i=0;i<repo.size();i++) {
           ArrayList<Integer> d = new ArrayList<>();
           d.add(convertToDate(repo.get(i).getPrNext()));
           d.add(convertToDate(repo.get(i).getF1Next()));
           d.add(convertToDate(repo.get(i).getF2Next()));
           d.add(convertToDate(repo.get(i).getF3Next()));
           d.add(convertToDate(repo.get(i).getF4Next()));
           d.add(convertToDate(repo.get(i).getF5Next()));
           numbers.add(d);
           careman.add(repo.get(i).getExploiterFio());
       }
        if (getView() != null) {
            listView = getView().findViewById(R.id.dashboard_list);
            View v = getView().findViewById(R.id.dashboard_constraint);
            final com.collector.kanatbek.erp.utils.ListAdapter adapter = new com.collector.kanatbek.erp.utils.ListAdapter(
                    getActivity()
                    , careman
                    , numbers);
            listView.setAdapter(adapter);
            searchText = v.findViewById(R.id.search_text);
            searchText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
                @Override
                public void afterTextChanged(Editable s) {
                    if (searchText.getText().length() > 0) {
                        searchTextFromList(searchText.getText() + "");
                    }  else {
                        initList();
                    }
                }
            });

            listView.setOnItemClickListener((parent, view, position, id) -> {
                Intent i = new Intent(getActivity(), ActivityDetailed.class);
                String exploiterFio = listView.getAdapter().getItem(position) + "";
                FilterChangeReport rep = new FilterChangeReport();
                for (int j=0;j<repo.size();j++) {
                    if (repo.get(j).getExploiterFio().equals(exploiterFio)) {
                        rep = repo.get(j);
                    }
                }

                i.putExtra("contractNumber", rep.getContractNumber());
                i.putExtra("sdate", rep.getSdate());
                i.putExtra("clientFio", rep.getExploiterFio());
                i.putExtra("serialNumber", rep.getSerialNumber());
                i.putExtra("caremanFio", rep.getCaremanFio());
                i.putExtra("customerId", rep.getCustomerId() + "");

                i.putExtra("f1MT", rep.getF1Mt() + "");
                i.putExtra("f2MT", rep.getF2Mt() + "");
                i.putExtra("f3MT", rep.getF3Mt() + "");
                i.putExtra("f4MT", rep.getF4Mt() + "");
                i.putExtra("f5MT", rep.getF5Mt() + "");
                i.putExtra("prMT", rep.getPrMt() + "");

                i.putExtra("f1Last", rep.getF1Last());
                i.putExtra("f1Next", rep.getF1Next());

                i.putExtra("f2Last", rep.getF2Last());
                i.putExtra("f2Next", rep.getF2Next());

                i.putExtra("f3Last", rep.getF3Last());
                i.putExtra("f3Next", rep.getF3Next());

                i.putExtra("f4Last", rep.getF4Last());
                i.putExtra("f4Next", rep.getF4Next());

                i.putExtra("f5Last", rep.getF5Last());
                i.putExtra("f5Next", rep.getF5Next());

                i.putExtra("prLast", rep.getPrLast());
                i.putExtra("prNext", rep.getPrNext());

                i.putExtra("address", rep.getAddress());
                i.putExtra("phones", rep.getPhone());

                startActivity(i);
            });
        }
    }

    public void initList() {
        final com.collector.kanatbek.erp.utils.ListAdapter adapter = new com.collector.kanatbek.erp.utils.ListAdapter(
                getActivity()
                , careman
                , numbers);
        listView.setAdapter(adapter);
    }

    public void searchTextFromList(String text) {
        text = text.toLowerCase();
        List<String> caremanCopy = new ArrayList<>();
        ArrayList<ArrayList<Integer>> numbersCopy = new ArrayList<>();

        if (careman != null && careman.size() > 0) {
            for (int i=0;i<careman.size();i++) {
                String caremanLowerCase = careman.get(i).toLowerCase();
                if (caremanLowerCase.contains(text)) {
                    caremanCopy.add(careman.get(i));
                    numbersCopy.add(numbers.get(i));
                }
            }
        }
        final com.collector.kanatbek.erp.utils.ListAdapter adapter = new com.collector.kanatbek.erp.utils.ListAdapter(
                getActivity()
                , caremanCopy
                , numbersCopy);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Integer convertToDate(String nextDate) {

        Date today = new Date();

        Calendar cal = Calendar.getInstance();

        cal.setTime(today);
        int currentYear = cal.get(Calendar.YEAR);
        String year = nextDate.split("-")[0];
        String month = nextDate.split("-")[1];
        int intYear = Integer.parseInt(year);
        int intMonth = Integer.parseInt(month);

        intMonth += intYear * 12;
        int currentMonth = currentYear * 12 + (cal.get(Calendar.MONTH) + 1);
        return currentMonth - intMonth;
    }

}
