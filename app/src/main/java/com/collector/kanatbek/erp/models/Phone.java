package com.collector.kanatbek.erp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Phone {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("phNumber")
    @Expose
    private String phNumber;

    public Phone(Integer id, String phNumber) {
        this.id = id;
        this.phNumber = phNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhNumber() {
        return phNumber;
    }

    public void setPhNumber(String phNumber) {
        this.phNumber = phNumber;
    }
}
