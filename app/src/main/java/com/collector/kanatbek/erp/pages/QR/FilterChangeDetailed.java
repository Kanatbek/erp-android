package com.collector.kanatbek.erp.pages.QR;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.RequiresApi;

import com.collector.kanatbek.erp.pages.List.ActivityDetailed;
import com.collector.kanatbek.erp.MainFragment;
import com.collector.kanatbek.erp.pages.Payment.PaymentActivity;
import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import com.collector.kanatbek.erp.R;
import com.collector.kanatbek.erp.models.SmService;
import com.collector.kanatbek.erp.Repository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FilterChangeDetailed extends Activity {

    ListView listView;
    ListView listView2;
    private String contractNumber = "";
    private String sn = "";
    private SmService smService = new SmService();

    private String mToken = "";
    private String m_Text = "";
    private String customerIin = "";
    private String customerId = "";

    private String phoneString;
    private List<String> phoneList = new ArrayList<>();
    private Button paymentBtn;
    private TextView paymentSumm;

    private List<String> filter_type = new ArrayList<>();
    private ArrayList<ArrayList<String>> detailed_list = new ArrayList<>();

    private List<String> arrayTitle = new ArrayList<>();
    private List<String> arrayText = new ArrayList<>();

    private TextView btnCancel;
    private TextView btnFinish;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_detailed);

        paymentBtn = findViewById(R.id.btn_payment);

        Toolbar toolbar = findViewById(R.id.toolbar);

        paymentSumm = findViewById(R.id.paymentSumm);

        btnCancel = findViewById(R.id.btn_cancel);
        btnFinish = findViewById(R.id.btn_finish);

        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        Intent intent = getIntent();

        initializeSmService_filterType();

        initializeDetailedList();

        fillFilterTypes();

        fillTerms(intent);

        fillLastDate(intent);

        fillNextDate(intent);

        if (intent != null) {
            if (intent.getStringExtra("contractNumber") != null) {
                arrayText.add(intent.getStringExtra("contractNumber"));
                contractNumber = intent.getStringExtra("contractNumber");
                arrayTitle.add("Договор №");
                smService.setContractNumber(intent.getStringExtra("contractNumber"));
            }
            if (intent.getStringExtra("sdate") != null) {
                arrayText.add(intent.getStringExtra("sdate"));
                arrayTitle.add("Дата договора");
            }
            if (intent.getStringExtra("clientFio") != null) {
                arrayText.add(intent.getStringExtra("clientFio"));
                arrayTitle.add("ФИО клиента");
            }
            if (intent.getStringExtra("serialNumber") != null) {
                arrayText.add(intent.getStringExtra("serialNumber"));
                arrayTitle.add("Товар-SN");
                sn = intent.getStringExtra("serialNumber");
            }
            if (intent.getStringExtra("caremanFio") != null) {
                arrayText.add(intent.getStringExtra("caremanFio"));
                arrayTitle.add("Careman");
            }
            if (intent.getStringExtra("address") != null) {
                arrayText.add(intent.getStringExtra("address"));
                arrayTitle.add("Адрес");
            }
            if (intent.getStringExtra("phones") != null) {
                phoneString = intent.getStringExtra("phones");
                getPhones();
            }
            if (intent.getStringExtra("iin") != null) {
                customerIin = intent.getStringExtra("iin");
            }
            if (intent.getStringExtra("customerId") != null) {
                customerId = intent.getStringExtra("customerId");
            }
            if (intent.getStringExtra("paymentSumm") != null) {
                paymentSumm.setText(intent.getStringExtra("paymentSumm") + " " + intent.getStringExtra("currency"));
            }
        }

        paymentBtn.setOnClickListener(v -> {
            Intent paymentIntent = new Intent(this, PaymentActivity.class);
            paymentIntent.putExtra("sn", sn);
            paymentIntent.putExtra("customerIin", customerIin);
            startActivity(paymentIntent);
        });

        listView2 = findViewById(R.id.first_list);
        final com.collector.kanatbek.erp.utils.List2Adapter adapter2 = new com.collector.kanatbek.erp.utils.List2Adapter(
                this
                , arrayTitle
                , arrayText);
        listView2.setAdapter(adapter2);

        listView = findViewById(R.id.detailed_list2);
        final com.collector.kanatbek.erp.utils.ListDetailedShortAdapter adapter = new com.collector.kanatbek.erp.utils.ListDetailedShortAdapter(
                this
                , filter_type
                , detailed_list
                , smService);
        listView.setAdapter(adapter);

        ActivityDetailed.ListUtils.setDynamicHeight(listView, 0);
        ActivityDetailed.ListUtils.setDynamicHeight(listView2, 150);
    }

    public void initializeSmService_filterType() {
        filter_type.add("PR");
        filter_type.add("F1");
        filter_type.add("F2");
        filter_type.add("F3");
        filter_type.add("F4");
        filter_type.add("F5");

        smService.setFno1(0);
        smService.setFno2(0);
        smService.setFno3(0);
        smService.setFno4(0);
        smService.setFno5(0);
        smService.setPr(0);
        smService.setPrCounter(0);
        smService.setFno1Counter(0);
        smService.setFno2Counter(0);
        smService.setFno3Counter(0);
        smService.setFno4Counter(0);
        smService.setFno5Counter(0);
    }

    public void callClicked(View view) {
        if (smService.getFno1() == 1
                || smService.getFno2() == 1
                || smService.getFno3() == 1
                || smService.getFno4() == 1
                || smService.getFno5() == 1
                || smService.getPr() == 1) {
            DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        textDialog();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Завершить процесс?").setPositiveButton("Да", dialogClickListener)
                    .setNegativeButton("Нет", dialogClickListener).show();
        }
    }

    public void endProcess(View view) {
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    Intent i = new Intent(FilterChangeDetailed.this, MainFragment.class);
                    i.putExtra("fragment", "qr");
                    startActivity(i);
                    resetSharedDetailed();
                    SharedPreferences.Editor pref = getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
                    pref.putString("barCode", "");
                    pref.apply();
                    pref.commit();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Отменить процесс?").setPositiveButton("Да", dialogClickListener)
                .setNegativeButton("Нет", dialogClickListener).show();
    }

    public void getHistory(View view) {
        Intent i = new Intent(FilterChangeDetailed.this, FilterChangeHistory.class);
        if (customerId != null && customerId.length() > 0) {
            i.putExtra("customerId", customerId);
        }
        startActivity(i);
    }

    public void textDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Заметка");
        builder.setCancelable(false);
        final EditText input = new EditText(this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", (dialog, which) -> {
            m_Text = input.getText().toString();
            postService(m_Text);
        });
        builder.show();
    }


    public void postService(String m_text) {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        mToken = explore.Explore(this, "TokenData", "token");
        String username = explore.Explore(this, "TokenData", "username");
        String startDay = explore.Explore(this, "TokenData", "start_day");
        String startTime = explore.Explore(this, "TokenData", "start_time");
        String longitude = explore.Explore(this, "TokenData", "longitude");
        String latitude = explore.Explore(this, "TokenData", "latitude");

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + mToken).build();
            return chain.proceed(request);
        });
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);

        String mCurrentTime;
        String mCurrentHour;
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);

        mCurrentTime = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) +1) + "-" + cal.get(Calendar.DAY_OF_MONTH);
        mCurrentHour = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);

        String type="";

        if (smService.getPrCounter() == 1) {
            type += "PROF";
        }
        if (smService.getFno1Counter() + smService.getFno2Counter() + smService.getFno3Counter()
                + smService.getFno4Counter() + smService.getFno5Counter() > 0) {
            type += "ZAMF";
        }
        if (type.equals("")) {
            Intent i = new Intent(FilterChangeDetailed.this, MainFragment.class);
            i.putExtra("fragment", "qr");
            startActivity(i);
        }
        Call<ResponseBody> call = repository.postNewService(contractNumber, mCurrentTime,
                                                            startDay + " " + startTime, mCurrentTime + " " + mCurrentHour,
                                                             smService.getFno1(),smService.getFno2(),
                                                             smService.getFno3(),smService.getFno4(),smService.getFno5(),
                                                            type, m_text, username, longitude, latitude);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Intent i = new Intent(FilterChangeDetailed.this, MainFragment.class);
                i.putExtra("fragment", "qr");
                startActivity(i);
                if (response.isSuccessful()) {
                    resetSharedDetailed();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void resetSharedDetailed() {
        SharedPreferences.Editor pref = getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
        pref.remove("SN");
        pref.remove("start_day");
        pref.remove("start_time");
        pref.remove("longitude");
        pref.remove("latitude");
        pref.apply();
        pref.commit();
    }

    public void getPhones() {
        int dotes = 0;
        for (int i=0;i<phoneString.length();i++) {
            if (String.valueOf(phoneString.charAt(i)).equals(",")) {
                dotes ++;
            }
        }
        for (int i=0;i<=dotes;i++) {
            phoneList.add(phoneString.split(",")[i]);
        }
    }

    public void initializeDetailedList() {
        detailed_list.add(new ArrayList<>());
        detailed_list.add(new ArrayList<>());
        detailed_list.add(new ArrayList<>());
        detailed_list.add(new ArrayList<>());
        detailed_list.add(new ArrayList<>());
        detailed_list.add(new ArrayList<>());
    }

    public void fillFilterTypes() {
        detailed_list.get(0).add("PR");
        detailed_list.get(1).add("F1");
        detailed_list.get(2).add("F2");
        detailed_list.get(3).add("F3");
        detailed_list.get(4).add("F4");
        detailed_list.get(5).add("F5");
    }

    public void fillTerms(Intent report) {
        if (report.getExtras() != null) {
            detailed_list.get(0).add(String.valueOf(report.getStringExtra("prMT")));
            detailed_list.get(1).add(String.valueOf(report.getStringExtra("f1MT")));
            detailed_list.get(2).add(String.valueOf(report.getStringExtra("f2MT")));
            detailed_list.get(3).add(String.valueOf(report.getStringExtra("f3MT")));
            detailed_list.get(4).add(String.valueOf(report.getStringExtra("f4MT")));
            detailed_list.get(5).add(String.valueOf(report.getStringExtra("f5MT")));
        }
    }

    public void fillLastDate(Intent report) {
        if (report.getExtras() != null) {
            detailed_list.get(0).add(report.getStringExtra("prLast"));
            detailed_list.get(1).add(report.getStringExtra("f1Last"));
            detailed_list.get(2).add(report.getStringExtra("f2Last"));
            detailed_list.get(3).add(report.getStringExtra("f3Last"));
            detailed_list.get(4).add(report.getStringExtra("f4Last"));
            detailed_list.get(5).add(report.getStringExtra("f5Last"));
        }
    }

    public void fillNextDate(Intent report) {
        if (report.getExtras() != null) {
            detailed_list.get(0).add(report.getStringExtra("prNext"));
            detailed_list.get(1).add(report.getStringExtra("f1Next"));
            detailed_list.get(2).add(report.getStringExtra("f2Next"));
            detailed_list.get(3).add(report.getStringExtra("f3Next"));
            detailed_list.get(4).add(report.getStringExtra("f4Next"));
            detailed_list.get(5).add(report.getStringExtra("f5Next"));
        }
    }

    @Override
    public void onBackPressed() {

    }
}
