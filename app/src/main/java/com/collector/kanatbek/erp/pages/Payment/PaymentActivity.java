package com.collector.kanatbek.erp.pages.Payment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.collector.kanatbek.erp.models.Contract;
import com.collector.kanatbek.erp.models.ResponseExample;
import com.collector.kanatbek.erp.pages.LockScreen.LockActivity;
import com.collector.kanatbek.erp.R;
import com.collector.kanatbek.erp.Repository;
import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import static java.security.AccessController.getContext;

public class PaymentActivity extends Activity {

    private TextView textFio;
    private TextView textTotalPayment;
    private TextView textPayment;
    private String sn = "";
    private String customerIin = "";
    private String mToken = "";
    private RecyclerView listView;
    private List<Contract> contracts = new ArrayList<>();
    private List<String> contractName = new ArrayList<>();
    private TextView contractTitle;
    private Button btnPay;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_activity);

        textFio = findViewById(R.id.text_fio);
        textTotalPayment = findViewById(R.id.text_totalPayment);
        textPayment = findViewById(R.id.text_payment);
        listView = findViewById(R.id.contract_recycleView);
        contractTitle = findViewById(R.id.contract_title);
        btnPay = findViewById(R.id.btn_pay);

        Intent intent = getIntent();
        if (intent != null) {
            sn = intent.getStringExtra("sn");
            customerIin = intent.getStringExtra("customerIin");
        }

        if (sn != null) {
            getContractBySN();
            getContractList();
        }

        btnPay.setOnClickListener(v -> {
            Intent intent1 = new Intent(this, LockActivity.class);
            intent1.putExtra("codeCheck", "codeCheck");
            startActivity(intent1);
        });
    }

    public void getContractList() {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        mToken = explore.Explore(this, "TokenData", "token");

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + mToken).build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        Call<ResponseExample> call = repository.getAllContractByIinBin(customerIin, "contractForMobileMini");
        call.enqueue(new Callback<ResponseExample>() {
            @Override
            public void onResponse(Call<ResponseExample> call, Response<ResponseExample> response) {
                if (response.isSuccessful()) {
                    contracts = new ArrayList<>();
                    contracts.addAll(response.body().embedded.contract);
                    contractName = new ArrayList<>();
                    for (int i=0;i<response.body().embedded.contract.size();i++) {
                        contractName.add(response.body().embedded.contract.get(i).getContractNumber() + " ");
                    }
                    if (contractName.size() > 0) {
                        fillContractList();
                    } else {
                        Toasty.warning(getApplicationContext(),
                                "Не найдено контрактов",
                                Toast.LENGTH_SHORT,
                                true).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseExample> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage() + " ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void fillContractList() {
        com.collector.kanatbek.erp.pages.Payment.ContractRecycleAdapter adapter
                = new com.collector.kanatbek.erp.pages.Payment.ContractRecycleAdapter(
                this
                , contractName
                , contracts
                , contractItem -> {
                    Intent intent = new Intent(this, ContractPaymentListActivity.class);
                    intent.putExtra("contractId", contractItem.getId() + "");
                    startActivity(intent);
                });

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        listView.setLayoutManager(layoutManager);

        listView.setAdapter(adapter);
    }

    public void getContractBySN() {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        mToken = explore.Explore(this, "TokenData", "token");

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + mToken).build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        Call<Contract> call = repository.getContractByInvSn(sn, "contractForMobileMini" +
                "");
        call.enqueue(new Callback<Contract>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<Contract> call, Response<Contract> response) {
                if (response.isSuccessful()) {
                    textFio.setText(response.body().getCustomer() != null
                            ? response.body().getCustomer().getFullFIO() : " ");
                    textTotalPayment.setText(response.body().getCost() + " " +
                            (response.body().getCurrency() != null ? response.body().getCurrency().getCurrency() : ""));
                    textPayment.setText((response.body().getPaid() != null ? response.body().getPaid() : 0)
                            + " " + (response.body().getCurrency() != null ? response.body().getCurrency().getCurrency() : ""));
                    contractTitle.setText(response.body().getInventory() != null ? response.body().getInventory().getName() : "Товар");
                } else {
                    if (getContext() != null) {
                        Toasty.warning(getApplicationContext(),
                                "Не найдено контрактов",
                                Toast.LENGTH_SHORT,
                                true).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<Contract> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}