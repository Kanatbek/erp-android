package com.collector.kanatbek.erp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterChangeReport implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("contract")
    @Expose
    private Contract contract;

    @SerializedName("contractNumber")
    @Expose
    private String contractNumber;

    @SerializedName("f1Mt")
    @Expose
    private Integer f1Mt;

    @SerializedName("f1Prev")
    @Expose
    private String f1Prev;

    @SerializedName("f1Last")
    @Expose
    private String f1Last;

    @SerializedName("f1Next")
    @Expose
    private String f1Next;

    @SerializedName("f2Mt")
    @Expose
    private Integer f2Mt;

    @SerializedName("f2Prev")
    @Expose
    private Object f2Prev;

    @SerializedName("f2Last")
    @Expose
    private String f2Last;

    @SerializedName("f2Next")
    @Expose
    private String f2Next;

    @SerializedName("f3Mt")
    @Expose
    private Integer f3Mt;

    @SerializedName("f3Prev")
    @Expose
    private String f3Prev;

    @SerializedName("f3Last")
    @Expose
    private String f3Last;

    @SerializedName("f3Next")
    @Expose
    private String f3Next;

    @SerializedName("f4Mt")
    @Expose
    private Integer f4Mt;

    @SerializedName("f4Prev")
    @Expose
    private String f4Prev;

    @SerializedName("f4Last")
    @Expose
    private String f4Last;

    @SerializedName("f4Next")
    @Expose
    private String f4Next;

    @SerializedName("f5Mt")
    @Expose
    private Integer f5Mt;

    @SerializedName("f5Prev")
    @Expose
    private String f5Prev;

    @SerializedName("f5Last")
    @Expose
    private String f5Last;
    @SerializedName("f5Next")
    @Expose
    private String f5Next;
    @SerializedName("prMt")
    @Expose
    private Integer prMt;
    @SerializedName("prPrev")
    @Expose
    private String prPrev;
    @SerializedName("prLast")
    @Expose
    private String prLast;
    @SerializedName("prNext")
    @Expose
    private String prNext;
    @SerializedName("enabled")
    @Expose
    private Boolean enabled;
    @SerializedName("cpuDate")
    @Expose
    private Object cpuDate;
    @SerializedName("exploiterFio")
    @Expose
    private String exploiterFio;
    @SerializedName("clientFio")
    @Expose
    private String clientFio;
    @SerializedName("caremanFio")
    @Expose
    private String caremanFio;
    @SerializedName("serialNumber")
    @Expose
    private String serialNumber;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("sdate")
    @Expose
    private String sdate;
    @SerializedName("customerId")
    @Expose
    private Integer customerId;

    @SerializedName("customerIin")
    @Expose
    private String customerIin;

    @SerializedName("payment")
    @Expose
    private Integer payment;

    @SerializedName("currency")
    @Expose
    private String currency;

    public FilterChangeReport() {

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Integer getF1Mt() {
        return f1Mt;
    }

    public void setF1Mt(Integer f1Mt) {
        this.f1Mt = f1Mt;
    }

    public String getF1Prev() {
        return f1Prev;
    }

    public void setF1Prev(String f1Prev) {
        this.f1Prev = f1Prev;
    }

    public String getF1Last() {
        return f1Last;
    }

    public void setF1Last(String f1Last) {
        this.f1Last = f1Last;
    }

    public String getF1Next() {
        return f1Next;
    }

    public void setF1Next(String f1Next) {
        this.f1Next = f1Next;
    }

    public Integer getF2Mt() {
        return f2Mt;
    }

    public void setF2Mt(Integer f2Mt) {
        this.f2Mt = f2Mt;
    }

    public Object getF2Prev() {
        return f2Prev;
    }

    public void setF2Prev(Object f2Prev) {
        this.f2Prev = f2Prev;
    }

    public String getF2Last() {
        return f2Last;
    }

    public void setF2Last(String f2Last) {
        this.f2Last = f2Last;
    }

    public String getF2Next() {
        return f2Next;
    }

    public void setF2Next(String f2Next) {
        this.f2Next = f2Next;
    }

    public Integer getF3Mt() {
        return f3Mt;
    }

    public void setF3Mt(Integer f3Mt) {
        this.f3Mt = f3Mt;
    }

    public String getF3Prev() {
        return f3Prev;
    }

    public void setF3Prev(String f3Prev) {
        this.f3Prev = f3Prev;
    }

    public String getF3Last() {
        return f3Last;
    }

    public void setF3Last(String f3Last) {
        this.f3Last = f3Last;
    }

    public String getF3Next() {
        return f3Next;
    }

    public void setF3Next(String f3Next) {
        this.f3Next = f3Next;
    }

    public Integer getF4Mt() {
        return f4Mt;
    }

    public void setF4Mt(Integer f4Mt) {
        this.f4Mt = f4Mt;
    }

    public String getF4Prev() {
        return f4Prev;
    }

    public void setF4Prev(String f4Prev) {
        this.f4Prev = f4Prev;
    }

    public String getF4Last() {
        return f4Last;
    }

    public void setF4Last(String f4Last) {
        this.f4Last = f4Last;
    }

    public String getF4Next() {
        return f4Next;
    }

    public void setF4Next(String f4Next) {
        this.f4Next = f4Next;
    }

    public Integer getF5Mt() {
        return f5Mt;
    }

    public void setF5Mt(Integer f5Mt) {
        this.f5Mt = f5Mt;
    }

    public String getF5Prev() {
        return f5Prev;
    }

    public void setF5Prev(String f5Prev) {
        this.f5Prev = f5Prev;
    }

    public String getF5Last() {
        return f5Last;
    }

    public void setF5Last(String f5Last) {
        this.f5Last = f5Last;
    }

    public String getF5Next() {
        return f5Next;
    }

    public void setF5Next(String f5Next) {
        this.f5Next = f5Next;
    }

    public Integer getPrMt() {
        return prMt;
    }

    public void setPrMt(Integer prMt) {
        this.prMt = prMt;
    }

    public String getPrPrev() {
        return prPrev;
    }

    public void setPrPrev(String prPrev) {
        this.prPrev = prPrev;
    }

    public String getPrLast() {
        return prLast;
    }

    public void setPrLast(String prLast) {
        this.prLast = prLast;
    }

    public String getPrNext() {
        return prNext;
    }

    public void setPrNext(String prNext) {
        this.prNext = prNext;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Object getCpuDate() {
        return cpuDate;
    }

    public void setCpuDate(Object cpuDate) {
        this.cpuDate = cpuDate;
    }

    public String getExploiterFio() {
        return exploiterFio;
    }

    public void setExploiterFio(String exploiterFio) {
        this.exploiterFio = exploiterFio;
    }

    public String getCaremanFio() {
        return caremanFio;
    }

    public void setCaremanFio(String caremanFio) {
        this.caremanFio = caremanFio;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSdate() {
        return sdate;
    }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public String getClientFio() {
        return clientFio;
    }

    public void setClientFio(String clientFio) {
        this.clientFio = clientFio;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCustomerIin() { return customerIin; }

    public void setCustomerIin(String customerIin) {
        this.customerIin = customerIin;
    }

    public Integer getPayment() { return payment; }

    public void setPayment(Integer payment) {
        this.payment = payment;
    }

    public String getCurrency() { return currency; }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.contractNumber);
        dest.writeString(this.sdate);
        dest.writeString(this.clientFio);
        dest.writeString(this.serialNumber);
        dest.writeString(this.caremanFio);
        dest.writeInt(this.customerId);
        dest.writeString(this.customerIin);

        dest.writeInt(this.f1Mt);
        dest.writeInt(this.f2Mt);
        dest.writeInt(this.f3Mt);
        dest.writeInt(this.f4Mt);
        dest.writeInt(this.f5Mt);
        dest.writeInt(this.prMt);
        dest.writeInt(this.payment);

        dest.writeString(this.f1Last);
        dest.writeString(this.f1Next);

        dest.writeString(this.f2Last);
        dest.writeString(this.f2Next);

        dest.writeString(this.f3Last);
        dest.writeString(this.f3Next);

        dest.writeString(this.f4Last);
        dest.writeString(this.f4Next);

        dest.writeString(this.f5Last);
        dest.writeString(this.f5Next);

        dest.writeString(this.prLast);
        dest.writeString(this.prNext);

        dest.writeString(this.address);
        dest.writeString(this.phone);
        dest.writeString(this.currency);
    }

    public FilterChangeReport(Parcel in) {
        if (in.readInt() == 0) {
            this.id = null;
        } else {
            this.id = in.readInt();
        }
        if (in.readInt() == 0) {
            this.payment = null;
        } else {
            this.payment = in.readInt();
        }
        contractNumber = in.readString();
        if (in.readByte() == 0) {
            this.f1Mt = null;
        } else {
            this.f1Mt = in.readInt();
        }
        this.f1Prev = in.readString();
        this.f1Last = in.readString();
        this.f1Next = in.readString();
        if (in.readByte() == 0) {
            this.f2Mt = null;
        } else {
            this.f2Mt = in.readInt();
        }
        this.f2Last = in.readString();
        this.f2Next = in.readString();
        if (in.readByte() == 0) {
            this.f3Mt = null;
        } else {
            this.f3Mt = in.readInt();
        }
        this.f3Prev = in.readString();
        this.f3Last = in.readString();
        this.f3Next = in.readString();
        if (in.readByte() == 0) {
            this.f4Mt = null;
        } else {
            this.f4Mt = in.readInt();
        }
        this.f4Prev = in.readString();
        this.f4Last = in.readString();
        this.f4Next = in.readString();
        if (in.readByte() == 0) {
            this.f5Mt = null;
        } else {
            this.f5Mt = in.readInt();
        }
        this.f5Prev = in.readString();
        this.f5Last = in.readString();
        this.f5Next = in.readString();
        if (in.readByte() == 0) {
            this.prMt = null;
        } else {
            this.prMt = in.readInt();
        }
        this.prPrev = in.readString();
        this.prLast = in.readString();
        this.prNext = in.readString();
        this.currency = in.readString();
        byte tmpEnabled = in.readByte();
        this.enabled = tmpEnabled == 0 ? null : tmpEnabled == 1;
        this.exploiterFio = in.readString();
        this.clientFio = in.readString();
        this.caremanFio = in.readString();
        this.serialNumber = in.readString();
        this.address = in.readString();
        this.phone = in.readString();
        this.sdate = in.readString();
        if (in.readByte() == 0) {
            this.customerId = null;
        } else {
            this.customerId = in.readInt();
        }
        this.customerIin = in.readString();
    }

    public static final Creator<FilterChangeReport> CREATOR = new Creator<FilterChangeReport>() {
        @Override
        public FilterChangeReport createFromParcel(Parcel in) {
            return new FilterChangeReport(in);
        }

        @Override
        public FilterChangeReport[] newArray(int size) {
            return new FilterChangeReport[size];
        }
    };
}
