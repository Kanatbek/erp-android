package com.collector.kanatbek.erp.utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.collector.kanatbek.erp.models.CollectorVisit;
import com.collector.kanatbek.erp.R;

import java.util.List;

public class SmVisitHistoryAdapter extends BaseAdapter{

        private Activity context;
        private List<CollectorVisit> visits;
        private static LayoutInflater inflater = null;
        private Integer partyId = 0;

        public SmVisitHistoryAdapter(Activity context, List<CollectorVisit> visits, Integer partyId) {
            this.context = context;
            this.visits = visits;
            this.partyId = partyId;
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

    @Override
    public int getCount() {
        return visits.size();
    }

    @Override
    public Object getItem(int position) {
        return visits.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;
            itemView = (itemView == null) ? inflater.inflate(R.layout.sm_visit_history_list, null) : itemView;
            TextView dateText = itemView.findViewById(R.id.visit_history_date);
            TextView customerFioText = itemView.findViewById(R.id.visit_history_customerFio);
            TextView timeText = itemView.findViewById(R.id.visit_history_time);


            if (position % 2 == 0) {
                itemView.setBackgroundColor(itemView.getResources().getColor(R.color.blurbackground));
            } else {
                itemView.setBackgroundColor(itemView.getResources().getColor(R.color.white));
            }
            CollectorVisit selectedVisit = visits.get(position);
            dateText.setText(selectedVisit.getDate());
            timeText.setText(selectedVisit.getVisitTime() + " мин");
            if (partyId != 0) {
                customerFioText.setText((selectedVisit.getCollectorFio() != null ) ? selectedVisit.getCollectorFio() : "-");
            } else {
                customerFioText.setText((selectedVisit.getCustomerFio() != null) ? selectedVisit.getCustomerFio() : "-");
            }
        return itemView;
    }
}
