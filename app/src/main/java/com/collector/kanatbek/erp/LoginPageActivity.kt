package com.collector.kanatbek.erp

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.collector.kanatbek.erp.models.Account
import com.collector.kanatbek.erp.models.Token
import com.collector.kanatbek.erp.models.User
import com.collector.kanatbek.erp.pages.LockScreen.LockActivity
import com.collector.kanatbek.erp.utils.ExploreSharedMemory
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_login.*

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Long
import java.util.*

class LoginPageActivity: AppCompatActivity() {
    private var token: String = ""
    private var sUsername: String = ""
    private var sToken: String = ""
    private var role = ""
    private var sRole: String = ""
    private val CAMERA_PERMISSION_CODE = 1

    override
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        spin_kit.visibility = View.INVISIBLE
        login_button.visibility = View.VISIBLE
        checkingTokenLifeTime()


        login_button.setOnClickListener {
            if (login!!.text!!.toString().length > 0
                    && password!!.text!!.toString().length > 0) {
                    getToken()
//                    spin_kit.setVisibility(View.VISIBLE)
                } else {
                Toasty.error(applicationContext, "Пароль/логин неправильно", Toast.LENGTH_LONG, true).show()
//                    Toast.makeText(applicationContext, "Пароль/логин неправильно", Toast.LENGTH_SHORT).show()
                    spin_kit.visibility= View.INVISIBLE
                    login_button.visibility = View.VISIBLE
                }
            }

        login_button.setOnLongClickListener() {
            login.setText("Kanat")
            password.setText("Cs2030")
            true
        }
    }

    private fun requestCameraPermission() {
        if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            AlertDialog.Builder(this)
                    .setTitle("Требуется разрешение")
                    .setMessage("Требуется разрешение для использования камеры")
                    .setPositiveButton("Разрешить") { dialog, which ->
                        ActivityCompat.requestPermissions(this@LoginPageActivity,
                                arrayOf(Manifest.permission.CAMERA), CAMERA_PERMISSION_CODE)
                    }
                    .create().show()
        } else
            ActivityCompat.requestPermissions(this@LoginPageActivity,
                    arrayOf(Manifest.permission.CAMERA), CAMERA_PERMISSION_CODE)
    }

    private fun requestLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            AlertDialog.Builder(this)
                    .setTitle("Требуется разрешение")
                    .setMessage("Требуется разрешение для использования карту")
                    .setPositiveButton("Разрешить") { dialog, which ->
                        ActivityCompat.requestPermissions(this@LoginPageActivity,
                                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), CAMERA_PERMISSION_CODE)
                    }
                    .create().show()
        } else
            ActivityCompat.requestPermissions(this@LoginPageActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), CAMERA_PERMISSION_CODE)
    }

    fun checkingTokenLifeTime() {
        val preferences = getSharedPreferences("TokenData", Context.MODE_PRIVATE)
        when {

            preferences.getString("loginDate", null) != null -> {
                val loginDateString = preferences.getString("loginDate", null)
                val tokenMillis = Long.valueOf(loginDateString!!)
                val currentDate = Date()
                val c = Calendar.getInstance()
                c.time = currentDate
                val currentMillis = c.timeInMillis
                val tokenDays = (tokenMillis / 1000 / 3600).toInt()
                Log.d("TokenMillis", tokenDays.toString());
                val currentDays = (currentMillis / 1000 / 3600).toInt()
                Log.d("CurrentDays", currentDays.toString());
                if (currentDays - tokenDays > 15) {
                    logOut()
                } else {
                    checkTokenAndNavigate()
                }
            }
        }
    }

    fun getToken() {
        val login = login.text.toString()
        val password = password.text.toString()
        spin_kit.visibility = View.VISIBLE
        login_button.visibility = View.INVISIBLE

        val okHttp = OkHttpClient.Builder()
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        okHttp.addInterceptor(logging)

        val builder = Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build())

        val retrofit = builder.build()
        val account = Account(login, password)
        val repository = retrofit.create(Repository::class.java)
        val call = repository.takeToken(account)

        call.enqueue(object : Callback<Token> {
            override fun onResponse(call: Call<Token>, response: Response<Token>?) {
                if (response!!.body() != null) {
                    val editor = getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit()
                    editor.putString("token", response.body().token)
                    editor.putString("username", login)

                    val loginDate = Date()
                    val c = Calendar.getInstance()
                    c.time = loginDate
                    val d = c.timeInMillis
                    editor.putString("loginDate", d.toString() + "")
                    editor.apply()
                    spin_kit.visibility = View.INVISIBLE
                    login_button.visibility = View.VISIBLE
                    getProfileData()
                } else {
                    Toasty.error(applicationContext, "Неверный пароль", Toast.LENGTH_LONG, true).show()
                    spin_kit.visibility = View.INVISIBLE
                    login_button.visibility = View.VISIBLE
                    logOut()
                }
            }
            override fun onFailure(call: Call<Token>, t: Throwable) {
                Toast.makeText(applicationContext, t.message + "onFailure1", Toast.LENGTH_SHORT).show()
                spin_kit.visibility = View.INVISIBLE
                login_button.visibility = View.VISIBLE
            }
        })
    }

    fun logOut() {
        getSharedPreferences("TokenData", Context.MODE_PRIVATE)
                            .edit()
                            .clear()
                            .apply()
        spin_kit.visibility = View.INVISIBLE
        login_button.visibility = View.VISIBLE
        return

//        val i = Intent(this, LoginPageActivity::class.java)
//        startActivity(i)
    }

    fun getProfileData() {
        val preferences = getSharedPreferences("TokenData", Context.MODE_PRIVATE)
        if (preferences.getString("token", null) != null) {
            sToken = preferences.getString("token", null)!!
        }
        if (preferences.getString("username", null) != null) {
            sUsername = preferences.getString("username", null)!!
        }
        spin_kit.visibility = View.VISIBLE
        login_button.visibility = View.INVISIBLE

        val okHttp = OkHttpClient.Builder()
        okHttp.addInterceptor { chain ->
            val request = chain.request()
                    .newBuilder()
                    .addHeader("Authorization", "Bearer $sToken")
                    .build()
            chain.proceed(request)
        }
        val builder = Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build())
        val retrofit = builder.build()
        val repository = retrofit.create(Repository::class.java)
        val call = repository.getUser(sUsername)
        call.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>?) {
                if (response!!.body() != null) {
                    val preferences1 = getSharedPreferences(
                            "TokenData", Context.MODE_PRIVATE).edit()
                    val d: User
                    if (response.body().userid != null) {
                        d = response.body()
                        when {
                            d.firstName != null -> preferences1.putString("firstname", d.firstName)
                        }

                        if (response.body().party != null) {
                                preferences1.putString("partyId", d.party.id!!.toString() + "")
                                preferences1.putString("email", d.party.email + "");
                                preferences1.putString("passportNumber", d.party.passportNumber + "");
                        } else {
                            Toasty.error(applicationContext, "Нет контрагента", Toast.LENGTH_LONG, true).show()
                            logOut()
                            return
                        }

                        when {
                            d.company != null -> {
                                preferences1.putString("companyId", d.company.id!!.toString() + "")
                                preferences1.putString("companyName", d.company.name + "")
                            }
                        }
                        when {
                            d.lastName != null -> preferences1.putString("lastname", d.lastName)
                        }
                        var role_careman = false
                        var role_collector = false
                        when {
                            d.userRoles != null -> {
                                var n = d.userRoles.size-1


                                while(n > 0) {
                                    if (d.userRoles.get(n) != null &&
                                            d.userRoles.get(n).name != null) {
                                            if (d.userRoles.get(n).name.equals("CAREMAN")) {
                                                role_careman = true
                                            }
                                            if (d.userRoles.get(n).name.equals("COLLECTOR")) {
                                                role_collector = true
                                            }
                                    }
                                    n--;
                                }
                                if (!role_careman  && role_collector) {
                                    preferences1.putString("role", "Collector")
                                    role = "Collector"
                                    }
                                else if (role_careman && !role_collector) {
                                    preferences1.putString("role", "Careman")
                                    role = "Careman"
                                    }
                                else if(role_careman && role_collector) {
                                    preferences1.putString("role", "Both")
                                    role = "Both"
                                    }
                            }
                        }
                        if(role_careman || role_collector) {
                            preferences1.apply()

                            val exploreSharedMemory = ExploreSharedMemory()
                            val savedUsername = exploreSharedMemory.Explore(this@LoginPageActivity, "CodeData", "username")
                            val newUsername = exploreSharedMemory.Explore(this@LoginPageActivity, "TokenData", "username")
                            val code = exploreSharedMemory.Explore(this@LoginPageActivity, "CodeData", "code")

                                if (code == "0" || savedUsername != newUsername) {
                                    val intent = Intent(this@LoginPageActivity, LockActivity::class.java)
                                    intent.putExtra("newCode", "newCode")
                                    startActivity(intent)
                                } else {
                                        val i = Intent(this@LoginPageActivity, MainFragment::class.java)
                                        startActivity(i)
                                }
                        } else {
                            Toasty.error(applicationContext, "Нет доступа", Toast.LENGTH_LONG, true).show()
                            spin_kit.visibility = View.INVISIBLE
                            login_button.visibility = View.VISIBLE
                        }

                    }
                }
            }
            override fun onFailure(call: Call<User>, t: Throwable) {
                Toast.makeText(applicationContext, t.message + " onFailure2", Toast.LENGTH_SHORT).show()
                spin_kit.visibility = View.INVISIBLE
                login_button.visibility = View.VISIBLE
            }
        })
    }

    fun checkTokenAndNavigate() {
        val preferences = getSharedPreferences("TokenData", Context.MODE_PRIVATE)
        if (preferences.getString("token", null) != null) {
            token = preferences.getString("token", null)!!
        }
        if (preferences.getString("role", null) != null) {
            sRole = preferences.getString("role", null)!!;
        }
        if (sRole.length > 0 && token != null) {
            val currenctFragment = getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit()
            currenctFragment.putString("currentFragment", "");
            currenctFragment.apply()
            currenctFragment.commit()
            val i = Intent(this@LoginPageActivity, MainFragment::class.java)
            startActivity(i)
        } else {
            logOut()
        }
        return
    }

    override fun onBackPressed() {
        //nothing
    }
}

