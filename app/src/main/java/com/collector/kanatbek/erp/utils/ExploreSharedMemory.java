package com.collector.kanatbek.erp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class ExploreSharedMemory {
        public String Explore(Activity activity, String sharedName, String title) {
            SharedPreferences preferences = activity.getSharedPreferences(sharedName, Context.MODE_PRIVATE);
            return (preferences.getString(title, null) != null) ? preferences.getString(title, null) : "0";
        }
}
