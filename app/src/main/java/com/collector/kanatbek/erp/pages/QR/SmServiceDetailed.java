package com.collector.kanatbek.erp.pages.QR;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.collector.kanatbek.erp.pages.List.ActivityDetailed;
import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import com.collector.kanatbek.erp.models.Contract;
import com.collector.kanatbek.erp.models.ContractPaymentSchedule;
import com.collector.kanatbek.erp.Repository;
import com.collector.kanatbek.erp.pages.Plan.AllVisitHistory;
import com.collector.kanatbek.erp.R;
import com.collector.kanatbek.erp.utils.SmServiceDetailedAdapter;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@SuppressLint("Registered")
public class SmServiceDetailed extends AppCompatActivity {

    ListView listView;
    ListView listView2;
    private String token;
    private String phoneString;
    private List<String> phones = new ArrayList<>();

    private List<String> detailed_list = new ArrayList<>();

    private List<String> arrayTitle = new ArrayList<>();
    private List<String> arrayText = new ArrayList<>();

    private Integer customerId = 0;
    private SpinKitView loadingIcon;

    private ArrayList<ContractPaymentSchedule> contractPaymentSchedules = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sm_service_detailed);

        setAllVisible(false);

        getContractById();

        Toolbar toolbar = findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    public void getHistory(View view) {
        Intent i = new Intent(SmServiceDetailed.this, AllVisitHistory.class);
        if (customerId != null ) {
            i.putExtra("partyId", customerId + "");
        }
        startActivity(i);
    }

    public void setAllVisible(Boolean allVisible) {
            ListView first_list = findViewById(R.id.first_list);
            View linearLayout = findViewById(R.id.linearLayout);
            ListView paymentList = findViewById(R.id.payment_list);
            SpinKitView loadingIcon = findViewById(R.id.loadingIcon);
            TextView txtView = findViewById(R.id.txtView);
            TextView text1 = findViewById(R.id.text1);

            if (allVisible) {
                first_list.setVisibility(View.VISIBLE);
                linearLayout.setVisibility(View.VISIBLE);
                paymentList.setVisibility(View.VISIBLE);
                txtView.setVisibility(View.VISIBLE);
                text1.setVisibility(View.VISIBLE);
                loadingIcon.setVisibility(View.INVISIBLE);

            } else {
                first_list.setVisibility(View.INVISIBLE);
                linearLayout.setVisibility(View.INVISIBLE);
                paymentList.setVisibility(View.INVISIBLE);
                txtView.setVisibility(View.INVISIBLE);
                text1.setVisibility(View.INVISIBLE);
                loadingIcon.setVisibility(View.VISIBLE);
            }
    }

    public void callClicked(View view) {
        CharSequence[] items = phones.toArray(new CharSequence[phones.size()]);

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Выберите номер");
        dialog.setItems(items, (dialog1, position) -> {
            String phone = phones.get(position);
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
            startActivity(intent);
        });
        dialog.setPositiveButton("Отмена", (dialog12, which) -> dialog12.dismiss());
        AlertDialog alert = dialog.create();
        alert.show();
    }

    public void getContractById() {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        token = explore.Explore(this, "TokenData", "token");

        String contractId = "";
        Intent intent = getIntent();
        if (intent.getStringExtra("contractId") != null){
            contractId = intent.getStringExtra("contractId");
        }

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + token).build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        Call<Contract> call = repository.getContractById(Integer.parseInt(contractId),"contractForMobile");
        call.enqueue(new Callback<Contract>() {
            @Override
            public void onResponse(Call<Contract> call, Response<Contract> response) {
                if (response.isSuccessful()) {
                    setAllVisible(true);
                    if (response.body() != null) {
                        customerId = response.body().getCustomer().getId();
                        fillContractTitles(response.body());
                        if (response.body().getContractPaymentSchedule() != null) {
                            contractPaymentSchedules.addAll(response.body().getContractPaymentSchedule());
                            int n = contractPaymentSchedules.size();
                            for (int i=0;i<n;i++) {
                                if (response.body().getCurrency() != null) {
                                    detailed_list.add(response.body().getCurrency().getCurrency());
                                } else {
                                    detailed_list.add("");
                                }
                            }
                        }
                        if (response.body().getPhonePay() != null) {
                            phones.add(response.body().getPhonePay().getPhNumber());
                        }
                        if (response.body().getMobilePay() != null) {
                            phones.add(response.body().getMobilePay().getPhNumber());
                        }
                    }
                } else {
                    Toasty.warning(getApplicationContext(),
                            "Не найден договор").show();
                }
                if (contractPaymentSchedules != null && contractPaymentSchedules.size() > 0) {
                    fillAdaptersTurnOffLoading();
                }
            }
            @Override
            public void onFailure(Call<Contract> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage() + " ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void fillContractTitles(Contract contract) {
        arrayTitle.add("Договор №");
        arrayTitle.add("Дата контракта");
        arrayTitle.add("Клиент");
        arrayTitle.add("Товар-SN");
        arrayTitle.add("Адрес");

        String contractNumber = "";
        String dateSigned = "";
        String customerName = "";
        String inventorySN = "";
        if (contract.getContractNumber() != null) {
            contractNumber = contract.getContractNumber();
        }

        if (contract.getDateSigned() != null) {
            dateSigned = contract.getDateSigned();
        }

        if (contract.getCustomer() != null &&
                contract.getCustomer().getFullFIO() != null) {
            customerName = contract.getCustomer().getFullFIO();
        }

        if (contract.getInventorySn() != null) {
            inventorySN = contract.getInventorySn();
        }
        arrayText.add(contractNumber);
        arrayText.add(dateSigned);
        arrayText.add(customerName);
        arrayText.add(inventorySN);

        String address = "";
        if (contract.getAddrPay() != null) {
            if (contract.getAddrPay().getInfo() != null) {
                address = contract.getAddrPay().getInfo();
            } else {
                address = "";
                if (contract.getAddrPay().getDistrict() != null) {
                    address += ("Район " + contract.getAddrPay().getDistrict() + ", ") ;
                }
                if (contract.getAddrPay().getStreet() != null) {
                    address += ("улица " + contract.getAddrPay().getStreet() + ", ");
                }
                if (contract.getAddrPay().getFlatNumber() != null) {
                    address += ("номер кв " + contract.getAddrPay().getFlatNumber() + ", ");
                }
                if (contract.getAddrPay().getHouseNumber() != null) {
                    address += ("номер дома " + contract.getAddrPay().getHouseNumber() + ", ");
                }
                if (contract.getAddrPay().getZipCode() != null) {
                    address += ("zipCode " + contract.getAddrPay().getZipCode() + ", ");
                }
            }
        }
        arrayText.add(address);
    }

    public void fillAdaptersTurnOffLoading() {
        listView2 = findViewById(R.id.first_list);

        final com.collector.kanatbek.erp.utils.List2Adapter adapter2
                = new com.collector.kanatbek.erp.utils.List2Adapter(
                this
                , arrayTitle
                , arrayText);
        listView2.setAdapter(adapter2);

        listView = findViewById(R.id.payment_list);

        final SmServiceDetailedAdapter adapter
                = new SmServiceDetailedAdapter(
                this
                , detailed_list
                , contractPaymentSchedules
                , "");
        listView.setAdapter(adapter);
        ActivityDetailed.ListUtils.setDynamicHeight(listView, 0);
        ActivityDetailed.ListUtils.setDynamicHeight(listView2, 100);
    }
}
