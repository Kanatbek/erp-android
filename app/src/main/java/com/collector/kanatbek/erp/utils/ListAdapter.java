package com.collector.kanatbek.erp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import com.collector.kanatbek.erp.*;

public class ListAdapter extends ArrayAdapter<String> {

    private Activity context;


    private ArrayList<ArrayList<Integer>> numbers;

    private List<String> careman;

    public ListAdapter(Activity context, List<String> careman, ArrayList<ArrayList<Integer>> numbers) {
        super(context, R.layout.mylist, careman);

        this.context = context;
        this.careman = careman;
        this.numbers = numbers;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        @SuppressLint({"ViewHolder", "InflateParams"}) View rowView = inflater.inflate(R.layout.mylist, null, true);
        if (position % 2 == 1) {
            rowView.setBackgroundColor(rowView.getResources().getColor(R.color.gray));
        } else {
            rowView.setBackgroundColor(rowView.getResources().getColor(R.color.white));
        }
        TextView client = rowView.findViewById(R.id.client_name);
        TextView id_names = rowView.findViewById(R.id.id_table);

        id_names.setText(position + 1 + "");
        client.setText(careman.get(position));

        TextView first = rowView.findViewById(R.id.number_first);
        TextView second = rowView.findViewById(R.id.number_second);
        TextView third = rowView.findViewById(R.id.number_third);
        TextView fourth = rowView.findViewById(R.id.number_fourth);
        TextView fifth = rowView.findViewById(R.id.number_fifth);
        TextView sixth =  rowView.findViewById(R.id.number_sixth);

        first.setText(numbers.get(position).get(0) + "");

        if (numbers.get(position).get(0) < 0) {
            first.setTextColor(getContext().getResources().getColor(R.color.orange));
        } else if (numbers.get(position).get(0) > 0){
            first.setTextColor(getContext().getResources().getColor(R.color.green));
        }

        if (numbers.get(position).get(1) < 0) {
            second.setTextColor(getContext().getResources().getColor(R.color.orange));
        } else if (numbers.get(position).get(1) > 0){
            second.setTextColor(getContext().getResources().getColor(R.color.green));
        }

        if (numbers.get(position).get(2) < 0) {
            third.setTextColor(getContext().getResources().getColor(R.color.orange));
        } else if (numbers.get(position).get(2) > 0){
            third.setTextColor(getContext().getResources().getColor(R.color.green));
        }

        if (numbers.get(position).get(3) < 0) {
            fourth.setTextColor(getContext().getResources().getColor(R.color.orange));
        } else if (numbers.get(position).get(3) > 0){
            fourth.setTextColor(getContext().getResources().getColor(R.color.green));
        }

        if (numbers.get(position).get(4) < 0) {
            fifth.setTextColor(getContext().getResources().getColor(R.color.orange));
        } else if (numbers.get(position).get(4) > 0){
            fifth.setTextColor(getContext().getResources().getColor(R.color.green));
        }

        if (numbers.get(position).get(5) < 0) {
            sixth.setTextColor(getContext().getResources().getColor(R.color.orange));
        } else if (numbers.get(position).get(5) > 0){
            sixth.setTextColor(getContext().getResources().getColor(R.color.green));
        }

        second.setText(numbers.get(position).get(1) + "");
        third.setText(numbers.get(position).get(2) + "");
        fourth.setText(numbers.get(position).get(3) + "");
        fifth.setText(numbers.get(position).get(4) + "");
        sixth.setText(numbers.get(position).get(5) + "");
        return rowView;
    }
}
