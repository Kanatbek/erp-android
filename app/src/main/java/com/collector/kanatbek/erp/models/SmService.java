package com.collector.kanatbek.erp.models;

public class SmService {
    private String contractNumber;

    private String sdate;
    private Integer prCounter;
    private Integer fno1Counter;
    private Integer fno2Counter;
    private Integer fno3Counter;
    private Integer fno4Counter;
    private Integer fno5Counter;

    private Integer pr;
    private Integer fno1;
    private Integer fno2;
    private Integer fno3;
    private Integer fno4;
    private Integer fno5;

    private Integer userId;


    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getSdate() {
        return sdate;
    }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public Integer getFno1() {
        return fno1;
    }

    public void setFno1(Integer fno1) {
        this.fno1 = fno1;
    }

    public Integer getFno2() {
        return fno2;
    }

    public void setFno2(Integer fno2) {
        this.fno2 = fno2;
    }

    public Integer getFno3() {
        return fno3;
    }

    public void setFno3(Integer fno3) {
        this.fno3 = fno3;
    }

    public Integer getFno4() {
        return fno4;
    }

    public void setFno4(Integer fno4) {
        this.fno4 = fno4;
    }

    public Integer getFno5() {
        return fno5;
    }

    public void setFno5(Integer fno5) {
        this.fno5 = fno5;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPr() {
        return pr;
    }

    public void setPr(Integer pr) {
        this.pr = pr;
    }

    public Integer getPrCounter() {
        return prCounter;
    }

    public void setPrCounter(Integer prCounter) {
        this.prCounter = prCounter;
    }

    public Integer getFno1Counter() {
        return fno1Counter;
    }

    public void setFno1Counter(Integer fno1Counter) {
        this.fno1Counter = fno1Counter;
    }

    public Integer getFno2Counter() {
        return fno2Counter;
    }

    public void setFno2Counter(Integer fno2Counter) {
        this.fno2Counter = fno2Counter;
    }

    public Integer getFno3Counter() {
        return fno3Counter;
    }

    public void setFno3Counter(Integer fno3Counter) {
        this.fno3Counter = fno3Counter;
    }

    public Integer getFno4Counter() {
        return fno4Counter;
    }

    public void setFno4Counter(Integer fno4Counter) {
        this.fno4Counter = fno4Counter;
    }

    public Integer getFno5Counter() {
        return fno5Counter;
    }

    public void setFno5Counter(Integer fno5Counter) {
        this.fno5Counter = fno5Counter;
    }
}
