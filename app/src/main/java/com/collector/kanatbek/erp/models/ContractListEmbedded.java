package com.collector.kanatbek.erp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContractListEmbedded {
    @SerializedName("contract")
    @Expose
    public List<Contract> contract = null;
}
