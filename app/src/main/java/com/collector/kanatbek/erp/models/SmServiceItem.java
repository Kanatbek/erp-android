package com.collector.kanatbek.erp.models;

public class SmServiceItem {
    private Integer fno;
    private Integer inventoryId;
    private Boolean isTovar;
    private String itemName;
    private Integer quentity;
    private Unit unit;


    public Integer getFno() {
        return fno;
    }

    public void setFno(Integer fno) {
        this.fno = fno;
    }

    public Integer getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Integer inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Boolean getTovar() {
        return isTovar;
    }

    public void setTovar(Boolean tovar) {
        isTovar = tovar;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getQuentity() {
        return quentity;
    }

    public void setQuentity(Integer quentity) {
        this.quentity = quentity;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}
