package com.collector.kanatbek.erp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.collector.kanatbek.erp.R;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class ListDetailedAdapter extends ArrayAdapter<String> {
    private Activity context;

    private ArrayList<ArrayList<String>> detailedList;

    public ListDetailedAdapter(Activity context, List<String> filters_type, ArrayList<ArrayList<String>> detailedList) {
        super(context, R.layout.my_detailed_list, filters_type);

        this.context = context;
        this.detailedList = detailedList;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        @SuppressLint({"ViewHolder", "InflateParams"}) View rowView = inflater.inflate(R.layout.my_detailed_list, null, true);

        TextView filterView = rowView.findViewById(R.id.filter_text);
        TextView termView = rowView.findViewById(R.id.srok_text);
        TextView lastDateView = rowView.findViewById(R.id.lastDate);
        TextView nextDateView = rowView.findViewById(R.id.nextDate);

            if (detailedList.get(position) != null && detailedList.get(position).size() > 3) {
                boolean isNull = false;
                for (int i=0;i<4;i++) {
                    if (detailedList.get(position).get(i) == null) {
                        isNull = true;
                    }
                }
                if (!isNull) {
                    filterView.setText(detailedList.get(position).get(0) + "");
                    termView.setText(detailedList.get(position).get(1) + "");
                    lastDateView.setText(detailedList.get(position).get(2) + "");
                    nextDateView.setText(detailedList.get(position).get(3) + "");

                    Date today = new Date();

                    Calendar cal = Calendar.getInstance();

                    cal.setTime(today);
                    int currentYear = cal.get(Calendar.YEAR);
                    String year = detailedList.get(position).get(3).split("-")[0] + "";
                    String month = detailedList.get(position).get(3).split("-")[1] + "";
                    int intYear = Integer.parseInt(year);
                    int intMonth = Integer.parseInt(month);

                    intMonth += intYear * 12;
                    int currentMonth = currentYear * 12 + (cal.get(Calendar.MONTH) + 1);

                    if (currentMonth < intMonth) {
                        nextDateView.setTextColor(getContext().getResources().getColor(R.color.green));
                    } else if (currentMonth > intMonth) {
                        nextDateView.setTextColor(getContext().getResources().getColor(R.color.red));
                    }
                }
        }
        return rowView;
    }
}
