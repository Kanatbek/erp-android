package com.collector.kanatbek.erp.pages.QR;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import com.collector.kanatbek.erp.models.History;
import com.collector.kanatbek.erp.models.SmServiceHistory;
import com.collector.kanatbek.erp.models.SmVisit;
import com.collector.kanatbek.erp.Repository;
import com.collector.kanatbek.erp.R;
import com.github.ybq.android.spinkit.SpinKitView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FilterChangeHistory extends Activity {

    private List<String> dates = new ArrayList<>();
    private List<String> itemNames = new ArrayList<>();
    private List<String> minutes = new ArrayList<>();
    private List<String> infos = new ArrayList<>();
    private SpinKitView loading;
    private List<SmServiceHistory> smServiceHistory = new ArrayList<>();
    private String sToken = "";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_change_history);
        loading = findViewById(R.id.history_loading);
        if (loading != null) {
            loading.setVisibility(View.VISIBLE);
        }

        Intent intent = getIntent();
        String customerId = intent.getStringExtra("customerId") != null ?
                            intent.getStringExtra("customerId") : "";
            getHistoryList(customerId);
    }

    public void getFromList() {
        int counter = 0;
        itemNames = new ArrayList<>();
        infos = new ArrayList<>();
        dates = new ArrayList<>();
        if (smServiceHistory != null && smServiceHistory.size() > 0) {
            for (int i=0;i<smServiceHistory.size();i++) {
                SmVisit visit;
                int itimeHours = 0;
                int itimeMinute = 0;
                int stimeHours = 0;
                int stimeMinute = 0;
                int minus = 0;
                if (smServiceHistory.get(i) != null
                        && smServiceHistory.get(i).getSmVisit() != null) {
                    visit = smServiceHistory.get(i).getSmVisit();
                    if (visit.getInTime() != null) {
                        itimeHours = Integer.parseInt(visit.getInTime().split(":")[0]);
                        itimeMinute = Integer.parseInt(visit.getInTime().split(":")[1]);
                    }
                    if (visit.getOutTime() != null) {
                        stimeHours = Integer.parseInt(visit.getOutTime().split(":")[0]);
                        stimeMinute = Integer.parseInt(visit.getOutTime().split(":")[1]);
                    }
                    if (visit.getInTime() != null
                            && visit.getOutTime() != null) {
                        minus = ((stimeHours * 60) + stimeMinute) - ((itimeHours * 60) + itimeMinute);
                    }
                }
                if (smServiceHistory.get(i) != null) {
                    if (smServiceHistory.get(i).getSdate() != null) {
                        dates.add(counter, smServiceHistory.get(i).getSdate());
                    }
                    if (smServiceHistory.get(i).getInfo() != null &&
                            smServiceHistory.get(i).getInfo().length() > 0) {
                        infos.add(counter, smServiceHistory.get(i).getInfo());
                    } else {
                        infos.add(counter, "Нет заметок.");
                    }
                    if (smServiceHistory.get(i).getsTime() != null && smServiceHistory.get(i).getiTime() != null) {
                        minutes.add(counter, minus + " мин");
                    } else {
                        minutes.add(counter, "-");
                    }
                    if (smServiceHistory.get(i).getType().equals("PROF")) {
                        itemNames.add(counter, "Профилактика");
                    } else if (smServiceHistory.get(i).getType().equals("REMT")) {
                        itemNames.add(counter, "Ремонт");
                    } else {
                        itemNames.add(counter, "Замена");
                    }
                }
                counter ++;
            }
            setLists();
        }
    }

    public void setLists() {
        ListView listView = findViewById(R.id.historyList);
        final com.collector.kanatbek.erp.utils.HistoryListAdapter adapter = new com.collector.kanatbek.erp.utils.HistoryListAdapter(
                this
                ,dates
                ,itemNames
                ,minutes
                ,infos);
        listView.setAdapter(adapter);
    }

    public void getHistoryList(String customerId) {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        sToken = explore.Explore(this, "TokenData", "token");
        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + sToken).build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        Call<History> call = repository.getHistory(Integer.parseInt(customerId), "mini");
        call.enqueue(new Callback<History>() {
            @Override
            public void onResponse(Call<History> call, Response<History> response) {
                if (response.isSuccessful()) {
                    if (response.body().getEmbedded() != null
                            && response.body().getEmbedded().getSmService().size() > 0) {
                        if (loading != null) {
                            loading.setVisibility(View.INVISIBLE);
                        }
                        smServiceHistory = new ArrayList<>();
                        if (response.body().getEmbedded().getSmService().size() > 0) {
                            smServiceHistory.addAll(response.body().getEmbedded().getSmService());
                            getFromList();
                        }
                    } else {
                        Toasty.info(getApplicationContext(), "Нет историй").show();
                    }
                }
            }

            @Override
            public void onFailure(Call<History> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
