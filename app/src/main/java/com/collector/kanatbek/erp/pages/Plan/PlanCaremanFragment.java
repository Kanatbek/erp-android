package com.collector.kanatbek.erp.pages.Plan;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import com.collector.kanatbek.erp.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import java.util.ArrayList;
import java.util.Objects;

public class PlanCaremanFragment extends Fragment {

    BarChart barChart;
    BarData BARDATA;
    BarDataSet Bardataset ;
    ArrayList<BarEntry> BARENTRY ;
    ArrayList<String> BarEntryLabels;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
       ExploreSharedMemory explore = new ExploreSharedMemory();
            int mPlan;
            int mFact;
            mPlan = Integer.parseInt(explore.Explore(getActivity(),"TokenData", "plan_careman"));
            mFact = Integer.parseInt(explore.Explore(getActivity(), "TokenData", "fact_careman"));
            fillPlan(mPlan, mFact);
    }

    public void fillPlan(Integer plan, Integer fact) {
        if (getView() != null) {
            barChart = Objects.requireNonNull(getActivity()).findViewById(R.id.chart);

            barChart.setTouchEnabled(false);
            barChart.setDoubleTapToZoomEnabled(false);
            barChart.setDragEnabled(false);

            // style chart
            barChart.setBackgroundColor(getResources().getColor(R.color.white)); // use your bg color
            barChart.setDescription(null);
            barChart.setDrawGridBackground(false);
            barChart.setDrawBorders(false);

            // remove axis
            YAxis leftAxis = barChart.getAxisLeft();
            leftAxis.setAxisMinimum(0);
            leftAxis.setEnabled(true);
            YAxis rightAxis = barChart.getAxisRight();
            rightAxis.setEnabled(false);
            XAxis xAxis = barChart.getXAxis();
            xAxis.setEnabled(false);
            BARENTRY = new ArrayList<>();
            BarEntryLabels = new ArrayList<>();

            AddValuesToBARENTRY(plan, fact);
            AddValuesToBarEntryLabels();
            Bardataset = new BarDataSet(BARENTRY, "plan / fact");
            BARDATA = new BarData(Bardataset);
            Bardataset.setColors(ColorTemplate.MATERIAL_COLORS);
            barChart.setData(BARDATA);
            barChart.animateY(900);
        }
    }

    public void AddValuesToBARENTRY(Integer plan, Integer fact){
        BARENTRY.add(new BarEntry(2f, plan));
        BARENTRY.add(new BarEntry(3f, fact));
    }

    public void AddValuesToBarEntryLabels(){
        BarEntryLabels.add("Plan");
        BarEntryLabels.add("Fact");
    }
}