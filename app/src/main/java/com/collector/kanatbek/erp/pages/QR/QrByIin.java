package com.collector.kanatbek.erp.pages.QR;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.collector.kanatbek.erp.pages.Payment.ContractListByIin;
import com.collector.kanatbek.erp.R;

import es.dmoral.toasty.Toasty;
import im.delight.android.location.SimpleLocation;

public class QrByIin extends Fragment {

    private Button continueBtn;
    private ImageView barImg;
    private EditText barText;
    private SimpleLocation location;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_iin, container, false);

        if (getActivity() != null) {
            location = new SimpleLocation(getActivity());

            SharedPreferences.Editor pref = getActivity().getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
            pref.putString("currentFragment", "careman_qr");
            pref.apply();
        }
        continueBtn = view.findViewById(R.id.continueBtn);
        barImg = view.findViewById(R.id.img2);
        barText = view.findViewById(R.id.iinText);

        barImg.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), QrCareman.class);
            startActivityForResult(intent, 2);
        });

        continueBtn.setOnClickListener(v -> {
            if (barText.getText().length() > 5) {
                SharedPreferences.Editor pref = getActivity().getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
                pref.putString("barCode", barText.getText().toString());
                pref.apply();
                pref.commit();
                Intent intent = new Intent(getContext(), ContractListByIin.class);
                intent.putExtra("iin", barText.getText().toString());

                startActivity(intent);
            } else {
                Toasty.warning(getActivity(),
                        "Заполните поле",
                        Toast.LENGTH_SHORT,
                        true).show();
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        String code = data.getStringExtra("qrCode");
        if (code != null && code.length() > 5){
            if (requestCode == 2) {
                barText.setText(code);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (location != null) {
            location.beginUpdates();
        }
    }

    @Override
    public void onPause() {
        if (location != null) {
            location.endUpdates();
        }
        super.onPause();
    }
}