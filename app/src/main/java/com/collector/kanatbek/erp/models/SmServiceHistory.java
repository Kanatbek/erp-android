package com.collector.kanatbek.erp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SmServiceHistory {

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("info")
    @Expose
    private String info;

    @SerializedName("customer")
    @Expose
    private Party customer;

    @SerializedName("sdate")
    @Expose
    private String sdate;

    @SerializedName("stime")
    @Expose
    private String sTime;

    @SerializedName("itime")
    @Expose
    private String iTime;

    @SerializedName("smServiceItems")
    @Expose
    private List<SmServiceItem> smServiceItemList;

    @SerializedName("caremanFio")
    @Expose
    private String caremanFio;

    @SerializedName("smVisit")
    @Expose
    private SmVisit smVisit;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Party getCustomer() {
        return customer;
    }

    public void setCustomer(Party customer) {
        this.customer = customer;
    }

    public String getSdate() { return sdate; }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public String getsTime() {
        return sTime;
    }

    public void setsTime(String sTime) {
        this.sTime = sTime;
    }

    public String getiTime() {
        return iTime;
    }

    public void setiTime(String iTime) {
        this.iTime = iTime;
    }

    public List<SmServiceItem> getSmServiceItemList() {
        return smServiceItemList;
    }

    public void setSmServiceItemList(List<SmServiceItem> smServiceItemList) {
        this.smServiceItemList = smServiceItemList;
    }

    public String getCaremanFio() {
        return caremanFio;
    }

    public void setCaremanFio(String caremanFio) {
        this.caremanFio = caremanFio;
    }

    public SmVisit getSmVisit() {
        return smVisit;
    }

    public void setSmVisit(SmVisit smVisit) {
        this.smVisit = smVisit;
    }
}
