package com.collector.kanatbek.erp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Currency {
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("currency")
    @Expose
    private String currency;

    public Currency() {

    }

    public Currency(String name, String currency) {
            this.name = name;
            this.currency = currency;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
