package com.collector.kanatbek.erp.pages.Payment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.collector.kanatbek.erp.models.Contract;
import com.collector.kanatbek.erp.models.ResponseExample;
import com.collector.kanatbek.erp.R;
import com.collector.kanatbek.erp.Repository;
import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import static java.security.AccessController.getContext;

public class ContractListByIin extends Activity {

    private ListView contractList;
    private List<String> contractNumbers;
    private List<Integer> contractSroks;
    private List<Integer> contractCosts;
    private List<Integer> contractPaids;
    private List<Integer> contractPayments;
    private String iin = "";
    private String mToken = "";
    private String currency = "";
    private String contractName = "";
    private List<Contract> contracts = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contract_list_by_iin);
        contractList = findViewById(R.id.contract_list_by_iin_id);

        Intent intent = getIntent();
        iin = intent.getStringExtra("iin") != null ? intent.getStringExtra("iin") : "";
        getContractsByIIN(iin);

        contractList.setOnItemClickListener((parent, view, position, id) -> {
            Intent paymentIntent = new Intent(this, PaymentActivity.class);
            paymentIntent.putExtra("sn", contracts.get(position).getInventorySn());
            String mIin = contracts.get(position).getCustomer() != null ? contracts.get(position).getCustomer().getIinBin(): iin;
            paymentIntent.putExtra("customerIin", mIin);
            startActivity(paymentIntent);
        });
    }

    public void getContractsByIIN(String iin) {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        mToken = explore.Explore(this, "TokenData", "token");

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + mToken).build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        Call<ResponseExample> call = repository.getAllContractByIinBin(iin, "contractForMobileMini");
        call.enqueue(new Callback<ResponseExample>() {
            @Override
            public void onResponse(Call<ResponseExample> call, Response<ResponseExample> response) {
                if (response.isSuccessful()
                        && response.body() != null
                        && response.body().embedded.contract.size() > 0) {
                    contracts = new ArrayList<>(response.body().embedded.contract);
                    fillContractList(contracts);
                    fillList();
                } else {
                    if (getContext() != null) {
                        Toasty.warning(getApplicationContext(),
                                "Не найдено контрактов",
                                Toast.LENGTH_SHORT,
                                true).show();
                        resetSharedDetailed();
                        SharedPreferences.Editor pref = getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
                        pref.putString("barCode", "");
                        pref.apply();
                        pref.commit();
                        onBackPressed();
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseExample> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage() + " ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void fillContractList(List<Contract> contracts) {
        contractNumbers = new ArrayList<>();
        contractSroks = new ArrayList<>();
        contractCosts = new ArrayList<>();
        contractPaids = new ArrayList<>();
        contractPayments = new ArrayList<>();
        for (int i=0;i<contracts.size();i++) {
            contractNumbers.add(contracts.get(i).getContractNumber());
            contractSroks.add(contracts.get(i).getMonth());
            contractCosts.add(contracts.get(i).getCost());
            contractPaids.add(contracts.get(i).getPaid());
            contractPayments.add(contracts.get(i).getCost() - contracts.get(i).getPaid());
            currency = contracts.get(i).getCurrency() != null ? contracts.get(i).getCurrency().getCurrency() : "$";
            contractName = contracts.get(i).getInventory() != null ?
                    contracts.get(i).getInventory().getName() : contracts.get(i).getContractNumber();
        }
    }


    public void resetSharedDetailed() {
        SharedPreferences.Editor pref = getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
        pref.remove("SN");
        pref.remove("start_day");
        pref.remove("start_time");
        pref.remove("longitude");
        pref.remove("latitude");
        pref.apply();
        pref.commit();
    }

    public void fillList() {
        com.collector.kanatbek.erp.pages.Payment.ContractListAdapter adapter
                = new com.collector.kanatbek.erp.pages.Payment.ContractListAdapter(
                this
                , contractNumbers
                , contractSroks
                , contractCosts
                , contractPaids
                , contractPayments
                , currency
                , contractName);
        contractList.setAdapter(adapter);
    }

}
