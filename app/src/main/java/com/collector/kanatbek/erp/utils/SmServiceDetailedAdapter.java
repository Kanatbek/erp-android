package com.collector.kanatbek.erp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.collector.kanatbek.erp.models.ContractPaymentSchedule;
import com.collector.kanatbek.erp.R;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class SmServiceDetailedAdapter extends ArrayAdapter<String> {
    private Activity context;
    private String str;
    private ArrayList<ContractPaymentSchedule> contractPaymentSchedule;
    private List<String> currency;
    private List<Integer> cashHistory = new ArrayList<>();
    private boolean isTaken = false;
    public SmServiceDetailedAdapter(Activity context,
                                List<String> currency,
                                ArrayList<ContractPaymentSchedule> contractPaymentSchedule,
                                String str) {
        super(context, R.layout.payment_schedule_list, currency);
        this.context = context;
        this.currency = currency;
        this.contractPaymentSchedule = contractPaymentSchedule;
        this.str = str;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        @SuppressLint({"ViewHolder", "InflateParams"}) View rowView = inflater.inflate(R.layout.payment_schedule_list, null, true);

        TextView orderView = rowView.findViewById(R.id.order_xml);
        TextView dateView = rowView.findViewById(R.id.date_xml);
        TextView summView= rowView.findViewById(R.id.summ_xml);
        TextView payView = rowView.findViewById(R.id.pay_xml);

        ContractPaymentSchedule schedule = contractPaymentSchedule.get(position);
        cashHistory = new ArrayList<>();
        for (int i=0;i<contractPaymentSchedule.size();i++) {
            cashHistory.add(contractPaymentSchedule.get(i).getPaid());
        }

        orderView.setText((position + 1) + "");
        dateView.setText(schedule.getPaymentDate());
        summView.setText(schedule.getSumm().toString() + " " + currency.get(position));
        payView.setText((schedule.getSumm() - schedule.getPaid()) + " " + currency.get(position));

        if (str.equals("QrEdit")) {
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int month = Calendar.getInstance().get(Calendar.MONTH);
            int day = Calendar.getInstance().get(Calendar.DATE);
                int mYear = Integer.parseInt(schedule.getPaymentDate().split("-")[0]);
                int mMonth = Integer.parseInt(schedule.getPaymentDate().split("-")[1]);
                int mDay = Integer.parseInt(schedule.getPaymentDate().split("-")[2]);
                drawBefore(rowView, year, month, day, mYear, mMonth, mDay, position);

        } else {
            if (position % 2 == 0) {
                rowView.setBackgroundColor(getContext().getResources().getColor(R.color.blurbackground));
            }
        }

        return rowView;
    }

    private void drawBefore(View view , int year, int month, int day,
                            int mYear, int mMonth, int mDay, final int position) {
        int CurrentTotalMonth = year * 12 + month;
        int ContractTotalMonth = mYear * 12 + mMonth;
        final TextView totalCash = context.findViewById(R.id.totalCash);

        if (CurrentTotalMonth - ContractTotalMonth >= 1) {
            view.setBackgroundColor(getContext().getResources().getColor(R.color.blackbackground));
        } else if (CurrentTotalMonth - ContractTotalMonth == 0) {
            ContractPaymentSchedule schedule = contractPaymentSchedule.get(position);
            if (schedule.getPaid() == schedule.getSumm()) {
                view.setBackgroundColor(getContext().getResources().getColor(R.color.blackbackground));
            } else {
                view.setOnClickListener(view1 -> showDialog(view1, position, totalCash));
            }
        } else if (CurrentTotalMonth - ContractTotalMonth < -2) {
            view.setBackgroundColor(getContext().getResources().getColor(R.color.blackbackground));
        } else {
            view.setOnClickListener(view12 -> showDialog(view12, position, totalCash));
        }
    }

    public void showDialog(View view, final Integer position, final TextView totalCash) {
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.collector_collecting_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        isTaken = false;
        TextView title = dialog.findViewById(R.id.dialog_collect_title);
        TextView mainText = dialog.findViewById(R.id.dialog_collect_mainText);
        final EditText editText = dialog.findViewById(R.id.dialog_collect_editText);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        Button btn = dialog.findViewById(R.id.dialog_closeBtn);
        btn.setOnClickListener(view1 -> {
            if (editText != null && editText.getText().toString().trim().length() > 0) {
                int edit = (editText.getText().toString().trim().length() > 0) ? Integer.parseInt(editText.getText().toString()) : 0;
                int total = (totalCash.getText().toString().trim().length() > 0) ? Integer.parseInt(totalCash.getText().toString()) : 0;
                if (edit + cashHistory.get(position) <= contractPaymentSchedule.get(position).getSumm()) {
                    isTaken = true;
                    cashHistory.set(position, cashHistory.get(position) + edit);
                    totalCash.setText((total  + edit) + " " + currency.get(position));
                    dialog.dismiss();
                } else {
                    Toast.makeText(getContext(), "Превышает лимит к оплате", Toast.LENGTH_SHORT).show();
                    view1.setBackgroundColor(getContext().getResources().getColor(R.color.succes_button));
                    isTaken = false;
                    dialog.dismiss();
                }
            }
        });
        title.setText("Собранная сумма");
        mainText.setText("Оплаченная сумма: " + cashHistory.get(position) + " " + currency.get(position) +  "\n"
                + "Собранная сумма: " + ((editText.getText().toString().trim().length() > 0) ? editText.getText().toString() : 0) + " " + currency.get(position));
        if (isTaken) {
            view.setBackgroundColor(getContext().getResources().getColor(R.color.succes_button));
        }
        dialog.show();
    }
}
