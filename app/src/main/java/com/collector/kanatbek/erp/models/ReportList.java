package com.collector.kanatbek.erp.models;


import java.util.List;

public class ReportList {

    private List<FilterChangeReport> reports;

    public List<FilterChangeReport> getReports() {
        return reports;
    }

    public void setReports(List<FilterChangeReport> reports) {
        this.reports = reports;
    }
}
