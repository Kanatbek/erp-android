package com.collector.kanatbek.erp.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Embedded {
        @SerializedName("smService")
        @Expose
        private List<SmServiceHistory> smService = null;

        public List<SmServiceHistory> getSmService() {
                return smService;
        }

        public void setSmService(List<SmServiceHistory> smService) {
                this.smService = smService;
        }
}