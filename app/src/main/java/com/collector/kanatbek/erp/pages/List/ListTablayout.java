package com.collector.kanatbek.erp.pages.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.collector.kanatbek.erp.R;
import com.google.android.material.tabs.TabLayout;

public class ListTablayout extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lists_fragment, container, false);

        if (getActivity() != null) {
            SharedPreferences.Editor pref = getActivity().getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
            pref.putString("currentFragment", "careman_List");
            pref.apply();

            ViewPager viewPager = view.findViewById(R.id.list_viewPager);

            viewPager.setAdapter(
                    new ListFragmentPagerAdapter(getChildFragmentManager(), getActivity()));

            TabLayout tabLayout = view.findViewById(R.id.sliding_tabs_list);

            tabLayout.setupWithViewPager(viewPager);

        }
        return view;
    }
}
