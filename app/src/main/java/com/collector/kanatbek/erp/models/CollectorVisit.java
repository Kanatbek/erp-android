package com.collector.kanatbek.erp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CollectorVisit implements Parcelable {

    @SerializedName("customerFio")
    @Expose
    private String customerFio;

    @SerializedName("collectorFio")
    @Expose
    private String collectorFio;

    @SerializedName("visitTime")
    @Expose
    private Integer visitTime;

    @SerializedName("summ")
    @Expose
    private Integer summ;

    @SerializedName("toCashbox")
    @Expose
    private Integer toCashbox;

    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("info")
    @Expose
    private String info;

    @SerializedName("inDate")
    @Expose
    private String date;

    public CollectorVisit() {

    }

    public CollectorVisit(String customerFio, String collectorFio, Integer visitTime, Integer summ,
                          Integer toCashbox, String currency, String info, String date) {
        this.customerFio = customerFio;
        this.collectorFio = collectorFio;
        this.visitTime = visitTime;
        this.summ = summ;
        this.toCashbox = toCashbox;
        this.currency = currency;
        this.info = info;
        this.date = date;
    }

    public String getCustomerFio() {
        return customerFio;
    }

    public void setCustomerFio(String customerFio) {
        this.customerFio = customerFio;
    }

    public String getCollectorFio() { return collectorFio; }

    public void setCollectorFio(String collectorFio) { this.collectorFio = collectorFio; }

    public Integer getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Integer visitTime) {
        this.visitTime = visitTime;
    }

    public Integer getSumm() {
        return summ;
    }

    public void setSumm(Integer summ) {
        this.summ = summ;
    }

    public Integer getToCashbox() {
        return toCashbox;
    }

    public void setToCashbox(Integer toCashbox) {
        this.toCashbox = toCashbox;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (this.customerFio != null) {
            dest.writeString(this.customerFio);
        }
        if (this.collectorFio != null) {
            dest.writeString(this.collectorFio);
        }
        if (this.visitTime != null) {
            dest.writeString(this.visitTime.toString());
        }
        if (this.summ != null) {
            dest.writeString(this.summ.toString());
        }
        if (this.toCashbox != null) {
            dest.writeString(this.toCashbox.toString());
        }
        if (this.currency != null) {
            dest.writeString(this.currency);
        }
        if (this.info != null) {
            dest.writeString(this.info);
        }
        if (this.date != null) {
            dest.writeString(this.date);
        }
    }
    public CollectorVisit (Parcel in) {
        this.customerFio = in.readString();
        this.collectorFio = in.readString();
        if (in.readByte() == 0) {
            this.visitTime= null;
        } else {
            this.visitTime = in.readInt();
        }
        if (in.readByte() == 0) {
            this.summ = null;
        } else {
            this.summ = in.readInt();
        }
        if (in.readByte() == 0) {
            this.toCashbox = null;
        } else {
            this.toCashbox = in.readInt();
        }
        this.currency = in.readString();
        this.info = in.readString();
        this.date = in.readString();
    }

    public static final Creator<CollectorVisit> CREATOR = new Creator<CollectorVisit>() {
        @Override
        public CollectorVisit createFromParcel(Parcel in) {return new CollectorVisit(in); }

        @Override
        public  CollectorVisit[] newArray(int size) {return new CollectorVisit[size]; }
    };
}


