package com.collector.kanatbek.erp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContractPaymentSchedule {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("paymentOrder")
    @Expose
    private Integer paymentOrder;

    @SerializedName("isFirstpayment")
    @Expose
    private Boolean isFirstpayment;

    @SerializedName("summ")
    @Expose
    private Integer summ;

    @SerializedName("paid")
    @Expose
    private Integer paid;

    @SerializedName("paymentDate")
    @Expose
    private String paymentDate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPaymentOrder() {
        return paymentOrder;
    }

    public void setPaymentOrder(Integer paymentOrder) {
        this.paymentOrder = paymentOrder;
    }

    public Boolean getFirstpayment() {
        return isFirstpayment;
    }

    public void setFirstpayment(Boolean firstpayment) {
        isFirstpayment = firstpayment;
    }

    public Integer getSumm() {
        return summ;
    }

    public void setSumm(Integer summ) {
        this.summ = summ;
    }

    public Integer getPaid() {
        return paid;
    }

    public void setPaid(Integer paid) {
        this.paid = paid;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }
}
