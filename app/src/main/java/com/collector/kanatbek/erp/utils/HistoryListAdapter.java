package com.collector.kanatbek.erp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.collector.kanatbek.erp.R;

import java.util.List;

public class HistoryListAdapter extends ArrayAdapter<String> {
    private Activity context;
    private List<String> dates;
    private List<String> itemNames;
    private List<String> times;
    private List<String> infos;

    public HistoryListAdapter(Activity context, List<String> dates, List<String> itemNames , List<String> minutes, List<String> infos) {
        super(context, R.layout.history_list, itemNames);
        this.context = context;
        this.dates = dates;
        this.itemNames = itemNames;
        this.times = minutes;
        this.infos = infos;
    }

    @NonNull
    public View getView(final int position, View view, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        @SuppressLint({"ViewHolder", "InflateParams"}) View rowView = inflater.inflate(R.layout.history_list, null, true);

        TextView sDate = rowView.findViewById(R.id.sdate);
        TextView itemName = rowView.findViewById(R.id.itemName);
        TextView time = rowView.findViewById(R.id.minutes);
        GridLayout itemNameView = rowView.findViewById(R.id.itemNameView);
        sDate.setText(dates.get(position));
        itemName.setText(itemNames.get(position));
        time.setText(times.get(position));
        itemNameView.setBackgroundColor(getContext().getResources().getColor(R.color.gray));

        rowView.setOnClickListener(v -> {
                AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                alertDialog.setTitle("Заметка");
                alertDialog.setMessage(infos.get(position));
                alertDialog.setIcon(R.drawable.info);
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", (dialog, which) -> {
                });
                alertDialog.show();
        });
        return rowView;

        }
    }