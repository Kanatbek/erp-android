package com.collector.kanatbek.erp.pages.LockScreen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.collector.kanatbek.erp.MainFragment;
import com.collector.kanatbek.erp.R;
import com.collector.kanatbek.erp.utils.ExploreSharedMemory;

import es.dmoral.toasty.Toasty;

public class LockActivity extends Activity {

    private ImageView i1, i2, i3, i4;
    private String code = "";
    private String newCode = "";
    private Boolean setNewCode = false;
    private Button btn1, btn2, btn3,
            btn4, btn5, btn6,
            btn7, btn8, btn9,
            btn10, btn11, btn12;
    private TextView lockTitle;
    private ExploreSharedMemory exploreSharedMemory;
    private String username = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lock_screen_activity);

        lockTitle = findViewById(R.id.lock_title);

        Intent intent = getIntent();
        newCode = intent.getStringExtra("codeChecking") != null ?
                        intent.getStringExtra("codeChecking") : "";
        if (intent.getStringExtra("newCode") != null) {
            lockTitle.setText("Введите новый pin-код");
            setNewCode = true;
        }
        if (intent.getStringExtra("codeChecking") != null) {
            lockTitle.setText("Повторите pin-код");
        }

        exploreSharedMemory = new ExploreSharedMemory();
        username = exploreSharedMemory.Explore(this, "TokenData", "username");

        i1 = findViewById(R.id.imageview_circle1);
        i2 = findViewById(R.id.imageview_circle2);
        i3 = findViewById(R.id.imageview_circle3);
        i4 = findViewById(R.id.imageview_circle4);

        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btn5 = findViewById(R.id.btn5);
        btn6 = findViewById(R.id.btn6);
        btn7 = findViewById(R.id.btn7);
        btn8 = findViewById(R.id.btn8);
        btn9 = findViewById(R.id.btn9);
        btn10 = findViewById(R.id.btn10);
        btn11 = findViewById(R.id.btn11);
        btn12 = findViewById(R.id.btn12);

        btn1.setOnClickListener(v -> {
            code += "1";
            codeImage(code);
        });
        btn2.setOnClickListener(v -> {
            code += "2";
            codeImage(code);
        });
        btn3.setOnClickListener(v -> {
            code += "3";
            codeImage(code);
        });
        btn4.setOnClickListener(v -> {
            code += "4";
            codeImage(code);
        });
        btn5.setOnClickListener(v -> {
            code += "5";
            codeImage(code);
        });
        btn6.setOnClickListener(v -> {
            code += "6";
            codeImage(code);
        });
        btn7.setOnClickListener(v -> {
            code += "7";
            codeImage(code);
        });
        btn8.setOnClickListener(v -> {
            code += "8";
            codeImage(code);
        });
        btn9.setOnClickListener(v -> {
            code += "9";
            codeImage(code);
        });
        btn11.setOnClickListener(v -> {
            code += "0";
            codeImage(code);
        });

        btn10.setOnClickListener(v -> {
            code = "";
            codeImage(code);
        });

        btn12.setOnClickListener(v -> {
            if (code.length() > 0) {
                code = code.substring(0, code.length() - 1);
                codeImage(code);
            }
        });
    }

    public void codeImage(String code) {
            if (code.length() == 1) {
                i1.setImageResource(R.drawable.circle2);
            } else {
                i2.setImageResource(R.drawable.circle);
                i3.setImageResource(R.drawable.circle);
                i4.setImageResource(R.drawable.circle);
            }

            if (code.length() == 2) {
                i1.setImageResource(R.drawable.circle2);
                i2.setImageResource(R.drawable.circle2);
            } else {
                i3.setImageResource(R.drawable.circle);
                i4.setImageResource(R.drawable.circle);
            }

            if (code.length() == 3) {
                i1.setImageResource(R.drawable.circle2);
                i2.setImageResource(R.drawable.circle2);
                i3.setImageResource(R.drawable.circle2);
            } else {
                i4.setImageResource(R.drawable.circle);
            }

            if (code.length() == 4) {
                i1.setImageResource(R.drawable.circle2);
                i2.setImageResource(R.drawable.circle2);
                i3.setImageResource(R.drawable.circle2);
                i4.setImageResource(R.drawable.circle2);
                checkCode(code);
            }

            if (code.length() == 0) {
                i1.setImageResource(R.drawable.circle);
                i2.setImageResource(R.drawable.circle);
                i3.setImageResource(R.drawable.circle);
                i4.setImageResource(R.drawable.circle);
            }
    }

    public void checkCode(String code) {
        if (newCode != null && newCode.length() > 0) {
            if (newCode.equals(code)) {
                SharedPreferences.Editor pref = getSharedPreferences("CodeData", Context.MODE_PRIVATE).edit();
                pref.putString("code", code);
                pref.putString("username", username + "");
                pref.apply();

                finish();
            } else {
                errorCode();
            }
        } else if (setNewCode){
            Intent intent = new Intent(this, LockActivity.class);
            intent.putExtra("codeChecking", code);
            startActivityForResult(intent, 2);
        } else {
            SharedPreferences preferences = getSharedPreferences("CodeData", Context.MODE_PRIVATE);
            String mCode = preferences.getString("code", null) != null ?
                        preferences.getString("code", null) : "";
            if (mCode != null &&  mCode.equals(code)) {
                Toasty.success(getApplicationContext(),
                        "Оплата успешно произведена",
                        Toast.LENGTH_SHORT,
                        true).show();
                SharedPreferences.Editor pref = getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
                pref.putString("currentFragment", "careman_plan");
                pref.putString("barCode", "");
                pref.apply();
                pref.commit();
                resetSharedDetailed();
                Intent intent = new Intent(this, MainFragment.class);
                startActivity(intent);
            } else {
                errorCode();
            }
        }
    }

    public void resetSharedDetailed() {
        SharedPreferences.Editor pref = getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
        pref.remove("SN");
        pref.remove("start_day");
        pref.remove("start_time");
        pref.remove("longitude");
        pref.remove("latitude");
        pref.apply();
        pref.commit();
    }

    public void errorCode() {
        code = "";
        i1.setImageResource(R.drawable.circle);
        i2.setImageResource(R.drawable.circle);
        i3.setImageResource(R.drawable.circle);
        i4.setImageResource(R.drawable.circle);
        Toasty.error(getApplicationContext(),
                "Код не совпадает",
                Toast.LENGTH_SHORT,
                true).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2) {
            Toasty.success(getApplicationContext(),
                    "Код сохранен",
                    Toast.LENGTH_SHORT,
                    true).show();
            Intent i = new Intent(LockActivity.this, MainFragment.class);
            startActivity(i);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
