package com.collector.kanatbek.erp.pages.List;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;

import com.collector.kanatbek.erp.pages.QR.FilterChangeHistory;
import com.collector.kanatbek.erp.R;

import java.util.ArrayList;
import java.util.List;

public class ActivityDetailed extends AppCompatActivity {

    ListView listView;
    ListView listView2;


    private String phoneString;
    private List<String> phones = new ArrayList<>();

    private List<String> filter_type = new ArrayList<>();
    private ArrayList<ArrayList<String>> detailed_list = new ArrayList<>();

    private List<String> arrayTitle = new ArrayList<>();
    private List<String> arrayText = new ArrayList<>();

    private String customerId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);

        Toolbar toolbar = findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        Intent intent = getIntent();

        filter_type.add("PR");
        filter_type.add("F1");
        filter_type.add("F2");
        filter_type.add("F3");
        filter_type.add("F4");
        filter_type.add("F5");

        initializeDetailedList();

        fillFilterTypes();

        fillTerms(intent);

        fillLastDate(intent);

        fillNextDate(intent);

        if (intent != null) {
            setItUp(intent,"contractNumber", "Договор №");
            setItUp(intent,"sdate", "Дата");
            setItUp(intent,"clientFio", "ФИО клиента");
            setItUp(intent,"serialNumber", "Товар-SN");
            setItUp(intent,"caremanFio", "Careman");
            setItUp(intent,"address", "Адрес");

            if (intent.getStringExtra("customerId") != null) {
                customerId = intent.getStringExtra("customerId");
            }

            if (intent.getStringExtra("phones") != null) {
                phoneString = intent.getStringExtra("phones");
                getPhones();
            }
        }

        listView2 = findViewById(R.id.first_list);

        final com.collector.kanatbek.erp.utils.List2Adapter adapter2 = new com.collector.kanatbek.erp.utils.List2Adapter(
                this
                , arrayTitle
                , arrayText);
        listView2.setAdapter(adapter2);

//        ------------------------------------------------------------------------------------------------------

        listView = findViewById(R.id.detailed_list2);

        final com.collector.kanatbek.erp.utils.ListDetailedAdapter adapter = new com.collector.kanatbek.erp.utils.ListDetailedAdapter(
                this
                , filter_type
                , detailed_list);
        listView.setAdapter(adapter);
        ListUtils.setDynamicHeight(listView, 0);
        ListUtils.setDynamicHeight(listView2, 150);
    }

    public void setItUp(Intent intent, String text, String title) {
        arrayText.add(intent.getStringExtra(text));
        arrayTitle.add(title);
    }

    public void getHistory(View view) {
        Intent i = new Intent(ActivityDetailed.this, FilterChangeHistory.class);
        if (customerId != null && customerId.length() > 0) {
            i.putExtra("customerId", customerId + "");
        }
        startActivity(i);
    }

    public void callClicked(View view) {
        CharSequence[] items = phones.toArray(new CharSequence[phones.size()]);

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Выберите номер");
        dialog.setItems(items, (dialog12, position) -> {
            String phone = phones.get(position);
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
            startActivity(intent);
        });
        dialog.setPositiveButton("Отмена", (dialog1, which) -> dialog1.dismiss());
        AlertDialog alert = dialog.create();
        alert.show();
    }

    public void getPhones() {
        int dotes = 0;
        for (int i=0;i<phoneString.length();i++) {
            if (String.valueOf(phoneString.charAt(i)).equals(",")) {
                dotes ++;
            }
        }
        for (int i=0;i<=dotes;i++) {
            phones.add(phoneString.split(",")[i]);
        }

    }

    public void initializeDetailedList() {
        detailed_list.add(new ArrayList<>());
        detailed_list.add(new ArrayList<>());
        detailed_list.add(new ArrayList<>());
        detailed_list.add(new ArrayList<>());
        detailed_list.add(new ArrayList<>());
        detailed_list.add(new ArrayList<>());
    }

    public void fillFilterTypes() {
        detailed_list.get(0).add("PR");
        detailed_list.get(1).add("F1");
        detailed_list.get(2).add("F2");
        detailed_list.get(3).add("F3");
        detailed_list.get(4).add("F4");
        detailed_list.get(5).add("F5");
    }

    public void fillTerms(Intent report) {
        if (report != null) {
            detailed_list.get(0).add(String.valueOf(report.getStringExtra("prMT")));
            detailed_list.get(1).add(String.valueOf(report.getStringExtra("f1MT")));
            detailed_list.get(2).add(String.valueOf(report.getStringExtra("f2MT")));
            detailed_list.get(3).add(String.valueOf(report.getStringExtra("f3MT")));
            detailed_list.get(4).add(String.valueOf(report.getStringExtra("f4MT")));
            detailed_list.get(5).add(String.valueOf(report.getStringExtra("f5MT")));
        }
    }

    public void fillLastDate(Intent report) {
        if (report.getExtras() != null) {
            detailed_list.get(0).add(report.getStringExtra("prLast"));
            detailed_list.get(1).add(report.getStringExtra("f1Last"));
            detailed_list.get(2).add(report.getStringExtra("f2Last"));
            detailed_list.get(3).add(report.getStringExtra("f3Last"));
            detailed_list.get(4).add(report.getStringExtra("f4Last"));
            detailed_list.get(5).add(report.getStringExtra("f5Last"));
        }
    }

    public void fillNextDate(Intent report) {
        if (report.getExtras() != null) {
            detailed_list.get(0).add(report.getStringExtra("prNext"));
            detailed_list.get(1).add(report.getStringExtra("f1Next"));
            detailed_list.get(2).add(report.getStringExtra("f2Next"));
            detailed_list.get(3).add(report.getStringExtra("f3Next"));
            detailed_list.get(4).add(report.getStringExtra("f4Next"));
            detailed_list.get(5).add(report.getStringExtra("f5Next"));
        }
    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView, Integer height) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                return;
            }
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }
}
