package com.collector.kanatbek.erp.models;

public class UserRole {
    private Integer roleId;
    private String name;
    private Boolean enable;
    private String label;


    public UserRole() {

    }

    public UserRole(Integer roleId) {
        this.roleId = roleId;
    }

    public UserRole(Integer roleId, String name, Boolean enable, String label) {
        this.roleId = roleId;
        this.name = name;
        this.enable = enable;
        this.label = label;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
