package com.collector.kanatbek.erp;

import com.collector.kanatbek.erp.models.Account;
import com.collector.kanatbek.erp.models.CollectorPlan;
import com.collector.kanatbek.erp.models.CollectorVisit;
import com.collector.kanatbek.erp.models.Contract;
import com.collector.kanatbek.erp.models.Employee;
import com.collector.kanatbek.erp.models.FilterChangeReport;
import com.collector.kanatbek.erp.models.History;
import com.collector.kanatbek.erp.models.PlanFact;
import com.collector.kanatbek.erp.models.ResponseExample;
import com.collector.kanatbek.erp.models.SmServiceList;
import com.collector.kanatbek.erp.models.Token;
import com.collector.kanatbek.erp.models.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Repository {

    @POST("/auth")
    Call<Token> takeToken(@Body Account account);

    @GET("/srsec/user/{username}")
    Call<User> getUser(@Path("username") String username);

    @GET("/smFilterChanges/loadSmFilterChanges?")
    Call<List<FilterChangeReport>> getCaremanReport(@Query("cid") Integer cid,
                                              @Query("bid") Integer bid,
                                              @Query("bool") Boolean bool,
                                              @Query("from") String from,
                                              @Query("to") String to,
                                              @Query("care") Integer care);

    @GET("/pfcon/getCaremanPlan?")
    Call<List<PlanFact>> getCaremanPlan(@Query("cid") Integer cid,
                                        @Query("bid") Integer bid,
                                        @Query("caremanId") Integer caremanId,
                                        @Query("dt") String dt);

    @GET("/pfcon/getCollectorPlan?")
    Call<List<CollectorPlan>> getCollectorPlan(@Query("cid") Integer cid,
                                         @Query("bid") Integer bid,
                                         @Query("pos") Integer pos,
                                         @Query("partyId") Integer partyId,
                                         @Query("dt") String dt);

    @GET("/employee/search/findCaremanByPid?")
    Call<Employee> getCaremanByParty(@Query("party") Integer party,
                                      @Query("projection") String projection);

    @GET("/employee/search/findCollectorByPid?")
    Call<Employee> getCollectorByParty(@Query("party") Integer party,
                                      @Query("projection") String projection);

    @GET("/smFilterChanges/loadSmFiChBySerNum")
    Call<FilterChangeReport> getReportBySN(@Query("ser_num") String ser_num);

    @POST("/smServices/newShortService?")
    Call<ResponseBody> postNewService(@Query("contractNumber") String contractNumber
                                        , @Query("sdate") String sDate
                                        , @Query("stime") String sTime
                                        , @Query("itime") String iTime
                                        , @Query("fno1") Integer fno1
                                        , @Query("fno2") Integer fno2
                                        , @Query("fno3") Integer fno3
                                        , @Query("fno4") Integer fno4
                                        , @Query("fno5") Integer fno5
                                        , @Query("type") String type
                                        , @Query("info") String info
                                        , @Query("username") String username
                                        , @Query("longitude") String longitude
                                        , @Query("latitude") String latitude);

    @GET("/smService/search/findAllByCustomer?")
    Call<History> getHistory(@Query("customer") Integer customer,
                             @Query("projection") String projection);

    @GET("/contracts/getCollectorForMobile?")
    Call<List<SmServiceList>> getServiceList(@Query("cid") Integer cid,
                                       @Query("bid") Integer bid,
                                       @Query("sid") Integer sid,
                                       @Query("dte") String dte);

    @GET("/contract/search/findcid?")
    Call<Contract> getContractById(@Query("id") Integer id,
                                   @Query("projection") String projection);

    @GET("/smVisitCollector/getVisitList?")
    Call<List<CollectorVisit>> getCollectorVisits(@Query("startDate") String startDate,
                                                  @Query("endDate") String endDate,
                                                  @Query("emplId") Integer employeeId);

    @GET("/smVisitCollector/getVisitByCustomer?")
    Call<List<CollectorVisit>> getCollectorVisitsByCustomer(@Query("startDate") String startDate,
                                                  @Query("endDate") String endDate,
                                                  @Query("emplId") Integer employeeId,
                                                  @Query("customerId") Integer customerId);

    @GET("/contract/search/getContractByInventorySn?")
    Call<Contract> getContractByInvSn(@Query("invSn") String invSn,
                                      @Query("projection") String projection);

    @POST("/smVisitCollector/newVisit?")
    Call<ResponseBody> postNewCollect(@Query("inDate") String inDate,
                                      @Query("inTime") String inTime,
                                      @Query("outTime") String outTime,
                                      @Query("emplId") Integer emplId,
                                      @Query("partyId") Integer partyId,
                                      @Query("summ") Integer summ,
                                      @Query("longitude") String longitude,
                                      @Query("latitude") String latitude,
                                      @Query("contractId") Integer contractId,
                                      @Query("info") String info);

    @GET("/contract/search/getAllContractsByIinBin?")
    Call<ResponseExample> getAllContractByIinBin(@Query("iinBin") String iinBin,
                                                 @Query("projection") String projection);

}
