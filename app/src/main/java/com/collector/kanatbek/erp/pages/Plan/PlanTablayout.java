package com.collector.kanatbek.erp.pages.Plan;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.collector.kanatbek.erp.pages.LockScreen.LockActivity;
import com.collector.kanatbek.erp.R;
import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import com.google.android.material.tabs.TabLayout;

public class PlanTablayout extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.plan_fragment, container, false);

        ExploreSharedMemory exploreSharedMemory = new ExploreSharedMemory();
        String savedUsername = exploreSharedMemory.Explore(getActivity(), "CodeData", "username");
        String newUsername = exploreSharedMemory.Explore(getActivity(), "TokenData", "username");
        String code = exploreSharedMemory.Explore(getActivity(), "CodeData", "code");

        if (getActivity() != null) {

            if (code.equals("0") || !savedUsername.equals(newUsername)) {
                Intent intent = new Intent(getActivity(), LockActivity.class);
                intent.putExtra("newCode", "newCode");
                startActivity(intent);
            }

            SharedPreferences.Editor pref = getActivity().getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
            pref.putString("currentFragment", "careman_plan");
            pref.apply();

            ViewPager viewPager = view.findViewById(R.id.plan_viewpager);

            viewPager.setAdapter(
                    new PlanFragmentPagerAdapter(getChildFragmentManager(), getActivity()));
            TabLayout tabLayout = view.findViewById(R.id.sliding_tabs_plan);
            tabLayout.setupWithViewPager(viewPager);
        }
        return view;
    }
}
