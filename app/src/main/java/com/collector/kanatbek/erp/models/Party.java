package com.collector.kanatbek.erp.models;

public class Party {
    private Integer id;
    private String firstname;
    private String lastname;
    private String middlename;
    private String fullFIO;
    private String photoString;
    private String iinBin;
    private String email;
    private String passportNumber;

    public Party(Integer id, String firstname, String lastname, String middlename,
                 String fullFIO, String photoString, String iinBin, String email, String passportNumber) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.fullFIO = fullFIO;
        this.photoString = photoString;
        this.iinBin = iinBin;
        this.email = email;
        this.passportNumber = passportNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getFullFIO() {
        return fullFIO;
    }

    public void setFullFIO(String fullFIO) {
        this.fullFIO = fullFIO;
    }

    public String getPhotoString() {
        return photoString;
    }

    public void setPhotoString(String photoString) {
        this.photoString = photoString;
    }

    public String getIinBin() {
        return iinBin;
    }

    public void setIinBin(String iinBin) {
        this.iinBin = iinBin;
    }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getPassportNumber() { return passportNumber; }

    public void setPassportNumber(String passportNumber) { this.passportNumber = passportNumber; }
}
