package com.collector.kanatbek.erp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Contract implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("customer")
    @Expose
    private Party customer;

    @SerializedName("company")
    @Expose
    private Company company;

    @SerializedName("branch")
    @Expose
    private Branch branch;

    @SerializedName("contractNumber")
    @Expose
    private String contractNumber;

    @SerializedName("currency")
    @Expose
    private Currency currency;

    @SerializedName("paymentSchedules")
    @Expose
    private List<ContractPaymentSchedule> contractPaymentSchedule;

    @SerializedName("dateSigned")
    @Expose
    private String dateSigned;

    @SerializedName("inventorySn")
    @Expose
    private String inventorySn;

    @SerializedName("addrPay")
    @Expose
    private AddrPay addrPay;

    @SerializedName("phonePay")
    @Expose
    private Phone phonePay;

    @SerializedName("mobilePay")
    @Expose
    private Phone mobilePay;

    @SerializedName("iinBin")
    @Expose
    private String iinBin;

    @SerializedName("summ")
    @Expose
    private Integer summ;

    @SerializedName("paid")
    @Expose
    private Integer paid;

    @SerializedName("month")
    @Expose
    private Integer month;

    @SerializedName("cost")
    @Expose
    private Integer cost;

    @SerializedName("inventory")
    @Expose
    private Inventory inventory;


    public Contract() { }

    public Contract(Integer id) {
        this.id = id;
    }

    public Contract(Integer id, Party customer, Company company,
                    Branch branch, String contractNumber,
                    Currency currency, String dateSigned, String inventorySn,
                    List<ContractPaymentSchedule> contractPaymentSchedule,
                    AddrPay addrPay, Phone phonePay, Phone mobilePay,
                    String iinBin, Integer summ, Integer paid, Integer month,
                    Integer cost, Inventory inventory) {
        this.id = id;
        this.customer = customer;
        this.company = company;
        this.branch = branch;
        this.contractNumber = contractNumber;
        this.currency = currency;
        this.contractPaymentSchedule = contractPaymentSchedule;
        this.dateSigned = dateSigned;
        this.inventorySn = inventorySn;
        this.addrPay = addrPay;
        this.phonePay = phonePay;
        this.mobilePay = mobilePay;
        this.iinBin = iinBin;
        this.summ = summ;
        this.paid = paid;
        this.month = month;
        this.cost = cost;
        this.inventory = inventory;
    }

    protected Contract(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        contractNumber = in.readString();
        dateSigned = in.readString();
        inventorySn = in.readString();
    }

    public static final Creator<Contract> CREATOR = new Creator<Contract>() {
        @Override
        public Contract createFromParcel(Parcel in) {
            return new Contract(in);
        }

        @Override
        public Contract[] newArray(int size) {
            return new Contract[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Party getCustomer() {
        return customer;
    }

    public void setCustomer(Party customer) {
        this.customer = customer;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getDateSigned() { return dateSigned;}

    public void setDateSigned(String dateSigned) { this.dateSigned = dateSigned;}

    public List<ContractPaymentSchedule> getContractPaymentSchedule() {
        return contractPaymentSchedule;
    }

    public void setContractPaymentSchedule(List<ContractPaymentSchedule> contractPaymentSchedule) {
        this.contractPaymentSchedule = contractPaymentSchedule;
    }

    public String getInventorySn() {
        return inventorySn;
    }

    public void setInventorySn(String inventorySn) {
        this.inventorySn = inventorySn;
    }

    public AddrPay getAddrPay() {
        return addrPay;
    }

    public void setAddrPay(AddrPay addrPay) {
        this.addrPay = addrPay;
    }

    public Phone getPhonePay() {
        return phonePay;
    }

    public void setPhonePay(Phone phonePay) {
        this.phonePay = phonePay;
    }

    public Phone getMobilePay() {
        return mobilePay;
    }

    public void setMobilePay(Phone mobilePay) {
        this.mobilePay = mobilePay;
    }

    public String getIinBin() { return iinBin; }

    public void setIinBin(String iinBin) { this.iinBin = iinBin; }

    public Integer getSumm() { return summ; }

    public void setSumm(Integer summ) { this.summ = summ; }

    public Integer getPaid() { return paid; }

    public void setPaid() { this.paid = paid; }

    public Integer getMonth() { return month; }

    public void setMonth(Integer month) { this.month = month; }

    public Integer getCost() { return cost; }

    public void setCost() { this.cost = cost; }

    public Inventory getInventory() { return inventory; }

    public void setInventory(Inventory inventory) { this.inventory = inventory; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(contractNumber);
        dest.writeString(dateSigned);
        dest.writeString(inventorySn);
    }
}
