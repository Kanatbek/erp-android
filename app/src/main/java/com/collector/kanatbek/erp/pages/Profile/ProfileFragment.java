package com.collector.kanatbek.erp.pages.Profile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import com.collector.kanatbek.erp.LoginPageActivity;
import com.collector.kanatbek.erp.R;
import com.collector.kanatbek.erp.models.User;
import com.collector.kanatbek.erp.Repository;
import java.util.Objects;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProfileFragment extends Fragment implements View.OnClickListener{

    private String token;
    private String username;

    private TextView fullName;

    private TextView branchTitle;
    private TextView branchName;

    private TextView companyTitle;
    private TextView companyName;

    private TextView positionName;
    private TextView positionTitle;

    private String fullFio;

    private Button logoutButton;

    private CircleImageView imageView;

    private String encodedImage = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        if (getActivity() != null) {
            SharedPreferences.Editor pref = getActivity().getSharedPreferences("TokenData", Context.MODE_PRIVATE).edit();
            pref.putString("currentFragment", "careman_Profile");
            pref.apply();
        }

        logoutButton = view.findViewById(R.id.button_logout);
        fullName = view.findViewById(R.id.fullFio);

        imageView = view.findViewById(R.id.profile_image);
        branchName = view.findViewById(R.id.branchNameCareman);
        branchTitle = view.findViewById(R.id.branchTitleCareman);

        companyName = view.findViewById(R.id.companyName);
        companyTitle = view.findViewById(R.id.companyTitle);

        positionName = view.findViewById(R.id.positionName);
        positionTitle = view.findViewById(R.id.positionTitle);

        logoutButton.setOnClickListener(this);
        SharedPreferences preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("TokenData", Context.MODE_PRIVATE);

        if (preferences.getString("firstname", null) == null) {
            getUserInfo();
        } else {
            if (preferences.getString("firstname", null) != null ) {
                fullFio = preferences.getString("firstname", null);
            }
            if (preferences.getString("lastname", null) != null) {
                fullFio += (" " + preferences.getString("lastname", null));
            }
            if (preferences.getString("photo", null) != null) {
                encodedImage = preferences.getString("photo", null);
            }
        }
        companyTitle.setText("Организация:");
        if (preferences.getString("companyName", null) != null) {
            companyName.setText(preferences.getString("companyName", null));
        }
        positionTitle.setText("Email:");
        String email = "";
        String passportNumber = "";

        email = preferences.getString("email", null).equals("null") ?
                "" : preferences.getString("email", null);

        passportNumber = preferences.getString("passportNumber", null).equals("null") ?
                                    "" : preferences.getString("passportNumber", null);

        positionName.setText(passportNumber);

        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        if (decodedByte != null) {
            imageView.setImageBitmap(decodedByte);
        }
        branchTitle.setText("Номер паспорта:");
//        if (branchTitle != null) {
//            if (preferences.getString("passportNumber", null).length() > 0) {
//                branchName.setText(preferences.getString("passportNumber", null) != null ?
//                                    preferences.getString("passportNumber", null) : "");
//            }
//        }
        branchName.setText(email);

        fullName.setText(fullFio);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_logout:
                logout();
                break;
        }
    }

    public void getUserInfo() {
        ExploreSharedMemory explore = new ExploreSharedMemory();

        token = explore.Explore(getActivity(), "TokenData", "token");
        username = explore.Explore(getActivity(), "TokenData", "username");

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request()
                    .newBuilder()
                    .addHeader("Authorization", "Bearer " + token)
                    .build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        Call<User> call = repository.getUser(username);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.body() != null) {
                    SharedPreferences.Editor preferences1 = Objects.requireNonNull(getActivity())
                                                            .getSharedPreferences(
                                                            "TokenData", Context.MODE_PRIVATE)
                                                            .edit();
                    fullFio = " ";
                    User d;
                    if (response.body().getUserid() != null) {
                        d = response.body();

                        if (d.getFirstName() != null) {
                            preferences1.putString("firstname", d.getFirstName());
                            fullFio = d.getFirstName();
                            preferences1.apply();
                        }
                        if (d.getLastName() != null) {
                            preferences1.putString("lastname", d.getLastName());
                            fullFio += " " + d.getLastName();
                            preferences1.commit();
                        }
                        fullName.setText(fullFio);
                    }
                } else {
                    if (getActivity() != null) {
                        (getActivity()).getSharedPreferences("TokenData", Context.MODE_PRIVATE)
                                .edit()
                                .clear()
                                .apply();
                        Intent i = new Intent(getActivity(), LoginPageActivity.class);
                        startActivity(i);
                    }
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                if (getContext() != null) {
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void logout() {
        DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    if (getActivity() != null) {
                        (getActivity()).getSharedPreferences("TokenData", Context.MODE_PRIVATE)
                                .edit()
                                .clear()
                                .apply();
                        Intent i = new Intent(getActivity(), LoginPageActivity.class);
                        startActivity(i);
                    }
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        builder.setMessage("Выйти из аккаунта?").setPositiveButton("Да", dialogClickListener)
                .setNegativeButton("Нет", dialogClickListener).show();
    }
}
