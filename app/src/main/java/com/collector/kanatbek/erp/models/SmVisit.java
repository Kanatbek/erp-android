package com.collector.kanatbek.erp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class SmVisit {

    @SerializedName("id")
    @Expose
    private Long id;

    @SerializedName("inTime")
    @Expose
    private String inTime;

    @SerializedName("outTime")
    @Expose
    private String outTime;

    @SerializedName("inDate")
    @Expose
    private String inDate;

    @SerializedName("longitude")
    @Expose
    private BigDecimal longitude;

    @SerializedName("latitude")
    @Expose
    private BigDecimal latitude;


    public SmVisit() { }

    public SmVisit(Long id) {
        this.id = id;
    }

    public SmVisit(Long id, String inTime, String outTime, String inDate, BigDecimal longitude, BigDecimal latitude) {
        this.id = id;
        this.inTime = inTime;
        this.outTime = outTime;
        this.inDate = inDate;
        this.longitude = longitude;
        this.latitude = latitude;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getInDate() {
        return inDate;
    }

    public void setInDate(String inDate) {
        this.inDate = inDate;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }



}
