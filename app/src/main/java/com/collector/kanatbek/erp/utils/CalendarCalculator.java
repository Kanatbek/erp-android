package com.collector.kanatbek.erp.utils;

public class CalendarCalculator {
    private Integer[] daysOfMonth1 = {31,28,31,30,31,30,31,31,30,31,30,31};
    private Integer[] daysOfMonth2 = {31,29,31,30,31,30,31,31,30,31,30,31};


    public Integer getMaxDateOfMonth(Integer year, Integer month) {
        if (year % 4 == 0) {
            return this.daysOfMonth2[month];
        } else {
            return this.daysOfMonth1[month];
        }
    }

}




