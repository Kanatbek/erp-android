package com.collector.kanatbek.erp.pages.List;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.collector.kanatbek.erp.utils.ExploreSharedMemory;
import com.collector.kanatbek.erp.models.SmServiceList;
import com.collector.kanatbek.erp.Repository;
import com.collector.kanatbek.erp.pages.QR.SmServiceDetailed;
import com.collector.kanatbek.erp.R;
import com.collector.kanatbek.erp.utils.SmServiceListAdapter;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListCollectorFragment extends Fragment {

    private String token = "";
    private Integer companyId = 0;
    private Integer branchId = 0;
    private Integer employeeId = 0;

    private ListView listView;
    public List<String> careman = new ArrayList<>();
    public ArrayList<ArrayList<Integer>> numbers = new ArrayList<>();
    private List<String> customerFIO = new ArrayList<>();
    private List<Integer> paymentDue = new ArrayList<>();
    private List<String> currency = new ArrayList<>();
    private View view;
    private EditText searchText;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main_list, container, false);

        SpinKitView spinKitView = view.findViewById(R.id.loading);
        spinKitView.setVisibility(View.VISIBLE);
        if (numbers != null && numbers.size() > 0) {
            //nothing
        } else {
            fillRealList();
        }
        setRetainInstance(true);
        return view;
    }

    public void fillRealList() {
        ExploreSharedMemory explore = new ExploreSharedMemory();
        Date today = new Date();

        Calendar cal = Calendar.getInstance();

        cal.setTime(today);

        String currentDate = "";
        currentDate += cal.get(Calendar.YEAR) + "-";
        currentDate += cal.get(Calendar.MONTH) +1  + "-";
        currentDate += cal.get(Calendar.DAY_OF_MONTH);

        token = explore.Explore(getActivity(), "TokenData", "token");
        companyId = Integer.parseInt(explore.Explore(getActivity(), "TokenData", "companyId"));
        branchId = Integer.parseInt(explore.Explore(getActivity(), "TokenData", "collectorBranchId"));
        employeeId = Integer.parseInt(explore.Explore(getActivity(), "TokenData", "collectorId"));

        OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
        okHttp.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + token).build();
            return chain.proceed(request);
        });

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://cordialtest.dyndns.org:12015")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttp.build());

        Retrofit retrofit = builder.build();
        final Repository repository = retrofit.create(Repository.class);
        Call<List<SmServiceList>> call = repository.getServiceList(companyId, branchId, employeeId,currentDate);
        call.enqueue(new Callback<List<SmServiceList>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<SmServiceList>> call, Response<List<SmServiceList>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().size() > 0) {
                        for (int i=0;i<response.body().size();i++) {
                            customerFIO.add(response.body().get(i).getCustomerFIO());
                            paymentDue.add(response.body().get(i).getPaymentDue());
                            currency.add(response.body().get(i).getCurrency());
                        }
                    }
                    setSmServiceList(response.body(), customerFIO, paymentDue, currency);
                        if (view != null) {
                            SpinKitView spinKitView = view.findViewById(R.id.loading);
                            spinKitView.setVisibility(View.INVISIBLE);
                        }
                } else {
                    if (getContext() != null) {
                        Toasty.error(getContext(),
                                "Сервис пуст",
                                Toast.LENGTH_SHORT,
                                true).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<List<SmServiceList>> call, Throwable t) {
                if (getActivity() != null) {
                    Toast.makeText(getActivity(), t.getMessage()+"", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void setSmServiceList(final List<SmServiceList> list, List<String> customerFIO, List<Integer> paymentDue, List<String> currency) {
        if (getView() != null) {
            listView = getView().findViewById(R.id.collector_dashboard_list);
            final SmServiceListAdapter adapter
                            =  new SmServiceListAdapter(
                    getActivity()
                    , customerFIO
                    , paymentDue
                    , currency);
            listView.setAdapter(adapter);
            searchText = getView().findViewById(R.id.textview_searchfield);
            searchText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
                @Override
                public void afterTextChanged(Editable s) {
                    if (searchText.getText().length() > 0) {
                        searchTextFromList(searchText.getText() + "");
                    }  else {
                        initList();
                    }
                }
            });

            listView.setOnItemClickListener((parent, view, position, id) -> {
                Intent i = new Intent(getActivity(), SmServiceDetailed.class);
                String customerFio = listView.getAdapter().getItem(position) + "";
                SmServiceList rep = new SmServiceList();
                for (int j=0;j<list.size();j++) {
                    if (list.get(j).getCustomerFIO().equals(customerFio)) {
                        rep = list.get(j);
                    }
                }
                i.putExtra("contractId", rep.getContractId() + "");
                startActivity(i);
            });
        }
    }

    public void initList() {
        final SmServiceListAdapter adapter = new SmServiceListAdapter(
                getActivity()
                , customerFIO
                , paymentDue
                , currency);
        listView.setAdapter(adapter);
    }

    public void searchTextFromList(String text) {
        text = text.toLowerCase();
        List<String> customerFioList = new ArrayList<>();
        List<Integer> numbersCopy = new ArrayList<>();
        List<String> currencyList = new ArrayList<>();

        if (customerFIO != null && customerFIO.size() > 0) {
            for (int i=0;i<customerFIO.size();i++) {
                String caremanLowerCase = customerFIO.get(i).toLowerCase();
                if (caremanLowerCase.contains(text)) {
                    customerFioList.add(customerFIO.get(i));
                    numbersCopy.add(paymentDue.get(i));
                    currencyList.add(currency.get(i));
                }
            }
        }
        final SmServiceListAdapter adapter
                        = new SmServiceListAdapter(
                getActivity()
                , customerFioList
                , numbersCopy
                , currencyList);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}