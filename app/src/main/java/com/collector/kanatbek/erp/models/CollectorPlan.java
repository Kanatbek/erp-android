package com.collector.kanatbek.erp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CollectorPlan {

    @SerializedName("plan")
    @Expose
    private Integer plan;

    @SerializedName("fact")
    @Expose
    private Integer fact;

    @SerializedName("currency")
    @Expose
    private Currency currency;

    public CollectorPlan() {

    }

    public Integer getPlan() {
        return plan;
    }

    public void setPlan(Integer plan) {
        this.plan = plan;
    }

    public Integer getFact() {
        return fact;
    }

    public void setFact(Integer fact) {
        this.fact = fact;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
